USE [master]
GO
/****** Object:  Database [CineCommunity]    Script Date: 02/07/2015 16:53:50 ******/
CREATE DATABASE [CineCommunity]
GO
ALTER DATABASE [CineCommunity] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CineCommunity].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CineCommunity] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CineCommunity] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CineCommunity] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CineCommunity] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CineCommunity] SET ARITHABORT OFF 
GO
ALTER DATABASE [CineCommunity] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CineCommunity] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CineCommunity] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CineCommunity] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CineCommunity] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CineCommunity] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CineCommunity] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CineCommunity] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CineCommunity] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CineCommunity] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CineCommunity] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CineCommunity] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CineCommunity] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CineCommunity] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CineCommunity] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CineCommunity] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CineCommunity] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CineCommunity] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CineCommunity] SET  MULTI_USER 
GO
ALTER DATABASE [CineCommunity] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CineCommunity] SET DB_CHAINING OFF 
GO
USE [CineCommunity]
GO
/****** Object:  Table [dbo].[T_Act_Media]    Script Date: 02/07/2015 16:53:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Act_Media](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[id_media] [varchar](30) NOT NULL,
	[id_person] [varchar](30) NOT NULL,
	[role] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_T_Act_Media] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Critic]    Script Date: 02/07/2015 16:53:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Critic](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[comment] [text] NULL,
	[score] [smallint] NOT NULL,
	[id_user] [bigint] NOT NULL,
	[id_media] [varchar](30) NOT NULL,
 CONSTRAINT [PK_T_Critic] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Episode]    Script Date: 02/07/2015 16:53:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Episode](
	[id_media] [varchar](30) NOT NULL,
	[number] [smallint] NOT NULL,
	[id_season] [varchar](30) NOT NULL,
 CONSTRAINT [PK_T_Episode] PRIMARY KEY CLUSTERED 
(
	[id_media] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Film]    Script Date: 02/07/2015 16:53:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Film](
	[id_media] [varchar](30) NOT NULL,
 CONSTRAINT [PK_T_Film] PRIMARY KEY CLUSTERED 
(
	[id_media] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Follow]    Script Date: 02/07/2015 16:53:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Follow](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[id_following] [varchar](30) NOT NULL,
	[id_follower] [bigint] NOT NULL,
	[is_following_person] [bit] NOT NULL,
 CONSTRAINT [PK_T_Follow] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_Friend_With]    Script Date: 02/07/2015 16:53:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Friend_With](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[id_user] [bigint] NOT NULL,
	[id_friend] [bigint] NOT NULL,
 CONSTRAINT [PK_T_Friend_With] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_Is_Type]    Script Date: 02/07/2015 16:53:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Is_Type](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[id_media] [varchar](30) NOT NULL,
	[id_type] [smallint] NOT NULL,
 CONSTRAINT [PK_T_Is_Type] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Media]    Script Date: 02/07/2015 16:53:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Media](
	[id] [varchar](30) NOT NULL,
	[name] [nvarchar](100) NOT NULL,
	[synopsis] [text] NOT NULL,
	[release_date] [date] NULL,
	[url_photo] [nchar](120) NULL,
	[rating] [float] NULL,
 CONSTRAINT [PK_T_Media] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Message]    Script Date: 02/07/2015 16:53:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Message](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[text] [text] NOT NULL,
	[id_user] [bigint] NOT NULL,
	[id_subject] [bigint] NOT NULL,
 CONSTRAINT [PK_T_Message] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_Person]    Script Date: 02/07/2015 16:53:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Person](
	[id] [varchar](30) NOT NULL,
	[name] [nvarchar](70) NOT NULL,
	[biography] [text] NULL,
	[height] [nchar](20) NULL,
	[birth_date] [date] NULL,
	[birth_place] [nchar](80) NULL,
	[url_photo] [nchar](120) NULL,
 CONSTRAINT [PK_T_Person] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Produce_Media]    Script Date: 02/07/2015 16:53:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Produce_Media](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[id_media] [varchar](30) NOT NULL,
	[id_person] [varchar](30) NOT NULL,
 CONSTRAINT [PK_T_Produce_Media] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Realise_Media]    Script Date: 02/07/2015 16:53:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Realise_Media](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[id_person] [varchar](30) NOT NULL,
	[id_media] [varchar](30) NOT NULL,
 CONSTRAINT [PK_T_Realise_Media] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Season]    Script Date: 02/07/2015 16:53:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Season](
	[id_media] [varchar](30) NOT NULL,
	[number] [smallint] NOT NULL,
	[end_date] [date] NULL,
	[id_serie] [varchar](30) NOT NULL,
 CONSTRAINT [PK_T_Season] PRIMARY KEY CLUSTERED 
(
	[id_media] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Serie]    Script Date: 02/07/2015 16:53:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Serie](
	[id_media] [varchar](30) NOT NULL,
	[end_date] [date] NULL,
 CONSTRAINT [PK_T_Serie] PRIMARY KEY CLUSTERED 
(
	[id_media] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Subject]    Script Date: 02/07/2015 16:53:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Subject](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](100) NOT NULL,
	[description] [text] NULL,
	[create_date] [datetime] NOT NULL,
 CONSTRAINT [PK_T_Subject] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_Type]    Script Date: 02/07/2015 16:53:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Type](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_T_Type] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_User]    Script Date: 02/07/2015 16:53:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_User](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](30) NOT NULL,
	[password] [nvarchar](100) NOT NULL,
	[gender] [bit] NULL,
	[birth_date] [date] NULL,
	[firstname] [nvarchar](50) NULL,
	[lastname] [nvarchar](50) NULL,
	[email] [nchar](200) NULL,
	[photo] [nvarchar](max) NULL,
 CONSTRAINT [PK_T_User] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_T_User] UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[T_Act_Media]  WITH CHECK ADD  CONSTRAINT [FK_T_Act_Media_T_Media] FOREIGN KEY([id_media])
REFERENCES [dbo].[T_Media] ([id])
GO
ALTER TABLE [dbo].[T_Act_Media] CHECK CONSTRAINT [FK_T_Act_Media_T_Media]
GO
ALTER TABLE [dbo].[T_Act_Media]  WITH CHECK ADD  CONSTRAINT [FK_T_Act_Media_T_Person] FOREIGN KEY([id_person])
REFERENCES [dbo].[T_Person] ([id])
GO
ALTER TABLE [dbo].[T_Act_Media] CHECK CONSTRAINT [FK_T_Act_Media_T_Person]
GO
ALTER TABLE [dbo].[T_Critic]  WITH CHECK ADD  CONSTRAINT [FK_T_Critic_T_Media] FOREIGN KEY([id_media])
REFERENCES [dbo].[T_Media] ([id])
GO
ALTER TABLE [dbo].[T_Critic] CHECK CONSTRAINT [FK_T_Critic_T_Media]
GO
ALTER TABLE [dbo].[T_Critic]  WITH CHECK ADD  CONSTRAINT [FK_T_Critic_T_User] FOREIGN KEY([id_user])
REFERENCES [dbo].[T_User] ([id])
GO
ALTER TABLE [dbo].[T_Critic] CHECK CONSTRAINT [FK_T_Critic_T_User]
GO
ALTER TABLE [dbo].[T_Episode]  WITH CHECK ADD  CONSTRAINT [FK_T_Episode_T_Media] FOREIGN KEY([id_media])
REFERENCES [dbo].[T_Media] ([id])
GO
ALTER TABLE [dbo].[T_Episode] CHECK CONSTRAINT [FK_T_Episode_T_Media]
GO
ALTER TABLE [dbo].[T_Episode]  WITH CHECK ADD  CONSTRAINT [FK_T_Episode_T_Season] FOREIGN KEY([id_season])
REFERENCES [dbo].[T_Season] ([id_media])
GO
ALTER TABLE [dbo].[T_Episode] CHECK CONSTRAINT [FK_T_Episode_T_Season]
GO
ALTER TABLE [dbo].[T_Film]  WITH CHECK ADD  CONSTRAINT [FK_T_Film_T_Media] FOREIGN KEY([id_media])
REFERENCES [dbo].[T_Media] ([id])
GO
ALTER TABLE [dbo].[T_Film] CHECK CONSTRAINT [FK_T_Film_T_Media]
GO
ALTER TABLE [dbo].[T_Friend_With]  WITH CHECK ADD  CONSTRAINT [FK_T_Friend_With_T_User] FOREIGN KEY([id_user])
REFERENCES [dbo].[T_User] ([id])
GO
ALTER TABLE [dbo].[T_Friend_With] CHECK CONSTRAINT [FK_T_Friend_With_T_User]
GO
ALTER TABLE [dbo].[T_Friend_With]  WITH CHECK ADD  CONSTRAINT [FK_T_Friend_With_T_User1] FOREIGN KEY([id_friend])
REFERENCES [dbo].[T_User] ([id])
GO
ALTER TABLE [dbo].[T_Friend_With] CHECK CONSTRAINT [FK_T_Friend_With_T_User1]
GO
ALTER TABLE [dbo].[T_Is_Type]  WITH CHECK ADD  CONSTRAINT [FK_T_Is_Type_T_Media] FOREIGN KEY([id_media])
REFERENCES [dbo].[T_Media] ([id])
GO
ALTER TABLE [dbo].[T_Is_Type] CHECK CONSTRAINT [FK_T_Is_Type_T_Media]
GO
ALTER TABLE [dbo].[T_Is_Type]  WITH CHECK ADD  CONSTRAINT [FK_T_Is_Type_T_Type] FOREIGN KEY([id_type])
REFERENCES [dbo].[T_Type] ([id])
GO
ALTER TABLE [dbo].[T_Is_Type] CHECK CONSTRAINT [FK_T_Is_Type_T_Type]
GO
ALTER TABLE [dbo].[T_Message]  WITH CHECK ADD  CONSTRAINT [FK_T_Message_T_Subject] FOREIGN KEY([id_subject])
REFERENCES [dbo].[T_Subject] ([id])
GO
ALTER TABLE [dbo].[T_Message] CHECK CONSTRAINT [FK_T_Message_T_Subject]
GO
ALTER TABLE [dbo].[T_Message]  WITH CHECK ADD  CONSTRAINT [FK_T_Message_T_User] FOREIGN KEY([id_user])
REFERENCES [dbo].[T_User] ([id])
GO
ALTER TABLE [dbo].[T_Message] CHECK CONSTRAINT [FK_T_Message_T_User]
GO
ALTER TABLE [dbo].[T_Produce_Media]  WITH CHECK ADD  CONSTRAINT [FK_T_Produce_Media_T_Media] FOREIGN KEY([id_media])
REFERENCES [dbo].[T_Media] ([id])
GO
ALTER TABLE [dbo].[T_Produce_Media] CHECK CONSTRAINT [FK_T_Produce_Media_T_Media]
GO
ALTER TABLE [dbo].[T_Produce_Media]  WITH CHECK ADD  CONSTRAINT [FK_T_Produce_Media_T_Person] FOREIGN KEY([id_person])
REFERENCES [dbo].[T_Person] ([id])
GO
ALTER TABLE [dbo].[T_Produce_Media] CHECK CONSTRAINT [FK_T_Produce_Media_T_Person]
GO
ALTER TABLE [dbo].[T_Realise_Media]  WITH CHECK ADD  CONSTRAINT [FK_T_Realise_Media_T_Media] FOREIGN KEY([id_media])
REFERENCES [dbo].[T_Media] ([id])
GO
ALTER TABLE [dbo].[T_Realise_Media] CHECK CONSTRAINT [FK_T_Realise_Media_T_Media]
GO
ALTER TABLE [dbo].[T_Realise_Media]  WITH CHECK ADD  CONSTRAINT [FK_T_Realise_Media_T_Person] FOREIGN KEY([id_person])
REFERENCES [dbo].[T_Person] ([id])
GO
ALTER TABLE [dbo].[T_Realise_Media] CHECK CONSTRAINT [FK_T_Realise_Media_T_Person]
GO
ALTER TABLE [dbo].[T_Season]  WITH CHECK ADD  CONSTRAINT [FK_T_Season_T_Media] FOREIGN KEY([id_media])
REFERENCES [dbo].[T_Media] ([id])
GO
ALTER TABLE [dbo].[T_Season] CHECK CONSTRAINT [FK_T_Season_T_Media]
GO
ALTER TABLE [dbo].[T_Season]  WITH CHECK ADD  CONSTRAINT [FK_T_Season_T_Serie] FOREIGN KEY([id_serie])
REFERENCES [dbo].[T_Serie] ([id_media])
GO
ALTER TABLE [dbo].[T_Season] CHECK CONSTRAINT [FK_T_Season_T_Serie]
GO
ALTER TABLE [dbo].[T_Serie]  WITH CHECK ADD  CONSTRAINT [FK_T_Serie_T_Media] FOREIGN KEY([id_media])
REFERENCES [dbo].[T_Media] ([id])
GO
ALTER TABLE [dbo].[T_Serie] CHECK CONSTRAINT [FK_T_Serie_T_Media]
GO
USE [master]
GO
ALTER DATABASE [CineCommunity] SET  READ_WRITE 
GO
