﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataVisualisationClient.DataAccess
{
    class PieData
    {
        public static KeyValuePair<string, int>[] GetMostLikedGenreByAge(string genre)
        {
            ServiceReference.ServiceClientCineCommunityClient client = new ServiceReference.ServiceClientCineCommunityClient();
            KeyValuePair<string, int>[] kvp = null;

            try
            {
                kvp = client.ComputeMostLikedGenreByAge(genre);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                throw;
            }

            return kvp;
        }
    }
}
