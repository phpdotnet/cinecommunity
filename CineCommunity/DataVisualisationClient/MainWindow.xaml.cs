﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DataVisualisationClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            List<string> data = new List<string>();
            data.Add("Fantasy");
            data.Add("Horror");

            Combo.ItemsSource = data;
            Combo.SelectedIndex = 0;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            string selectedValue = Combo.SelectedItem as string;
            KeyValuePair<string, int>[] kvp =  await Task.Factory.StartNew(() =>
            {
                return new BusinessManagement.PieData().GetMostLikedGenreByAge(selectedValue);
            });

            if (kvp != null)
                pieChart.DataContext = kvp;
        }
    }
}
