﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataVisualisationClient.BusinessManagement
{
    class PieData
    {
        public KeyValuePair<string, int>[] GetMostLikedGenreByAge(string genre)
        {
            return DataAccess.PieData.GetMostLikedGenreByAge(genre);
        }
    }
}
