/**
 * Created by Antec on 25/06/2015.
 */
$(function () {
    //This section will run whenever we call Chat.cshtml page
    var chat = $.connection.chatHub;
    //Create a function that the hub can call back to display messages
    chat.client.addNewMessageToChat = function (subjectId, username, message) {
        var currentSubjectId = $("#chat input[name=id]").val();
        if (subjectId == currentSubjectId)
            $("#chat .messages").append('<li><strong>' + htmlEncode(username) + '</strong>: ' + htmlEncode(message) + '</li>');
    };
    chat.client.addNewSubject = function(id, title, description) {
        var list_subjects = $("#subjects");
        list_subjects.append('<li onclick="openNewChat(' + id + ', ' + title + ')><strong>'
                + title + '</strong><p>' + description + '</p></li>');
    }
    var openNewChat = function (subjectId, title) {
        showChat();

        var messages = $("#chat .messages");
        messages.empty();
        $("#chat input[name=id]").val(subjectId);
        $("#chat .title").html(title);

        chat.server.getMessages(subjectId);
        closeMenuChat();
    };
    chat.client.openNewChat = openNewChat;
    chat.client.loadSubjects = function(subjects) {
        var list_subjects = $("#subjects");
        for (var i = 0; i < subjects.length; ++i) {
            var subject = subjects[i];
            var component = document.createElement("li");
            component.onclick = (function(id, title) {
                return function () { openNewChat(id, title); };
            })(subject.id, subject.title);
            var titleComponent = document.createElement("strong");
            titleComponent.innerHTML = htmlEncode(subject.title);
            component.appendChild(titleComponent);
            var descriptionComponent = document.createElement("p");
            descriptionComponent.innerHTML = htmlEncode(subject.description);
            component.appendChild(descriptionComponent);
            list_subjects.append(component);
            //list_subjects.append("<li onclick=\"openNewChat(" + subject.id + ", " + htmlEncode(subject.title) + ")><strong>" + htmlEncode(subject.title) + "</strong><p>" + htmlEncode(subject.description) + "</p></li>");
        }
    };
    chat.client.loadMessages = function (messages) {
        var list_messages = $("#chat .messages");
        for (var i = 0; i < messages.length; i++) {
            var username = messages[i].username;
            var message = messages[i].text;
            list_messages.append('<li><strong>' + htmlEncode(username) + '</strong>: ' + htmlEncode(message) + '</li>');
        }
    }
    //Function for displayin a hello message on connection
    chat.client.onConnected = function (id, title, userName, UserID, content) {
        var strWelcome = 'Welcome: ' + userName;
        $("#chat .content").append('<li>' + content + '<br/>' + strWelcome + '<br/></li>');

        $("#chat input[name=id]").val(id);
        $("#chat input[name=userId]").val(UserID);
        $("#chat input[name=username]").val(userName);
        $("#chat .title").innerHtml = title;

    }


    $.connection.hub.start().done(function () {
        $("#chat textarea[name=message]").keypress(function (e) {
            if (e.which == 13) {
                var msg = $("#chat textarea[name=message]").val();
                if (msg.length > 0) {
                    var id = $("#chat input[name=id]").val();
                    var username = $('#chat input[name=username]').val();
                    // <<<<<-- ***** Return to Server [  SendMessage  ] *****
                    chat.server.send(id, username, msg);

                    $("#chat textarea[name=message]").val('').focus();
                }
            }
        });
        $("#create_subject").click(function(e) {
            var title = $("#title_subject");
            var description = $("#description_subject");
            var user = $("#creator_subject").val();
            chat.server.createSubject(title.val(), description.val(), user);
            title.val('');
            description.val('');
            closeCreateSubject();
        });
        chat.server.getSubjects();
    });
});

function htmlEncode(value) {
    var encodedValue = $('<div />').text(value).html();
    return encodedValue;
}


function minimizeChat(chat) {
    var content = chat.getElementsByClassName('content')[0];
    if (content.classList.contains('hidden'))
        content.classList.remove('hidden');
    else
        content.classList.add('hidden');
}

function closeChat(chat) {
    chat.parentNode.removeChild(chat);
}

function openMenuChat() {
    var menuChat = document.getElementById('chat_menu');
    if (menuChat.classList.contains('hidden'))
        menuChat.classList.remove('hidden');
    else
        menuChat.classList.add('hidden');
}

function closeMenuChat() {
    var menuChat = document.getElementById('chat_menu');
    menuChat.classList.add('hidden');
}

function openCreateSubject() {
    var subjectCreation = document.getElementById('subject_creation');
    if (subjectCreation.classList.contains('hidden'))
        subjectCreation.classList.remove('hidden');
    else
        subjectCreation.classList.add('hidden');
}

function closeCreateSubject() {
    document.getElementById('subject_creation').classList.add('hidden');
}

function showChat() {
    var chat = document.getElementById('chat');
    if (chat.classList.contains('hidden'))
        chat.classList.remove('hidden');
}