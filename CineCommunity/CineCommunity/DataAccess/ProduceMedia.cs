﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.DataAccess
{
    public class ProduceMedia
    {
        public bool Create(Dbo.ProduceMedia produce_media)
        {
            T_Produce_Media t_produce_media = new T_Produce_Media()
            {
                id = produce_media.Id,
                id_person = produce_media.IdPerson,
                id_media = produce_media.IdMedia,
            };
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    ctx.T_Produce_Media.Add(t_produce_media);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Update(Dbo.ProduceMedia produce_media)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Produce_Media t_produce_media = ctx.T_Produce_Media.Where(x => x.id == produce_media.Id).SingleOrDefault();
                    t_produce_media.id_person = produce_media.IdPerson;
                    t_produce_media.id_media = produce_media.IdMedia;

                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }
    }
}