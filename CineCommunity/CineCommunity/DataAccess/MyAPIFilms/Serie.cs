﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace CineCommunity.DataAccess.MyAPIFilms
{
    public class Serie : ApiHandler
    {
        public static Dbo.Serie GetSimpleData(JObject item)
        {
            JToken tokenTemp = null;
            Dbo.Serie serie = new Dbo.Serie();

            item.TryGetValue("idIMDB", out tokenTemp);

            serie.Id = ((JValue)tokenTemp).Value<string>().Substring(2);

            item.TryGetValue("title", out tokenTemp);
            serie.Name = ((JValue)tokenTemp).Value<string>();

            item.TryGetValue("plot", out tokenTemp);
            serie.Synopsis = ((JValue)tokenTemp).Value<string>();


            item.TryGetValue("urlPoster", out tokenTemp);
            serie.UrlPhoto = ((JValue)tokenTemp).Value<string>();


            item.TryGetValue("releaseDate", out tokenTemp);
            serie.ReleaseDate = (((JValue)tokenTemp).Value<string>() == "") ?
                                    default(DateTime) : DateTime.ParseExact(((JValue)tokenTemp).Value<string>(),
                                                                    "yyyyMMdd",
                                                                    System.Globalization.CultureInfo.InvariantCulture);

            item.TryGetValue("year", out tokenTemp);

            serie.EndDate = (((JValue)tokenTemp).Value<string>() == "" || ((JValue)tokenTemp).Value<string>().Split('–').Length == 1) ?
                                default(DateTime) : DateTime.ParseExact(((JValue)tokenTemp).Value<string>().Split('–')[1] + "0101",
                                                                        "yyyyMMdd",
                                                                         System.Globalization.CultureInfo.InvariantCulture);

            item.TryGetValue("genres", out tokenTemp);
            serie.Types = new List<string>();
            foreach (var genre in (JArray)tokenTemp)
            {
                string genreValue = ((JValue)genre).Value.ToString();
                serie.Types.Add(genreValue);
            }


            item.TryGetValue("rating", out tokenTemp);
            serie.Rating = (((JValue)tokenTemp).Value<string>().Length == 0) ? 0 : ((JValue)tokenTemp).Value<double>();
            
            return serie;
        }

        public static async Task<Dbo.Serie> GetSerieById(string serieId)
        {
            Dbo.Serie serie = new Dbo.Serie();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://www.myapifilms.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("imdb?idIMDB=tt" + serieId + "&format=JSON&aka=0&business=0&seasons=0&seasonYear=0&technical=0&lang=en-us&actors=N&biography=0&trailer=0&uniqueName=0&filmography=0&bornDied=0&starSign=0&actorActress=0&actorTrivia=0&movieTrivia=0&awards=0&moviePhotos=N&movieVideos=N&similarMovies=0&token=" + Serie._token).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    string s = await response.Content.ReadAsStringAsync();
                    JObject json = JsonConvert.DeserializeObject<JObject>(s);

                    serie = GetSimpleData(json);
                }

            }

            return serie;
        }

        public static async Task<Dictionary<String, List<Dbo.Person>>> GetDetailledInfo(string serieId)
        {
            Dictionary<String, List<Dbo.Person>> dict = new Dictionary<string, List<Dbo.Person>>() {
                { "Actor", new List<Dbo.Person>() },
                { "Director", new List<Dbo.Person>() }
            };

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://www.myapifilms.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("imdb?idIMDB=tt" + serieId + "&format=JSON&aka=0&business=0&seasons=0&seasonYear=0&technical=0&lang=en-us&actors=S&biography=0&trailer=0&uniqueName=0&filmography=0&bornDied=0&starSign=0&actorActress=0&actorTrivia=0&movieTrivia=0&awards=0&moviePhotos=N&movieVideos=N&similarMovies=0&token=" + Serie._token).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    string s = await response.Content.ReadAsStringAsync();
                    JObject json = JsonConvert.DeserializeObject<JObject>(s);

                    JArray actors = json["actors"].Value<JArray>();

                    foreach (var item in actors)
                    {
                        Dbo.Person p = new Dbo.Person()
                        {
                            Id = item["actorId"].Value<string>().Substring(2),
                            Name = item["actorName"].Value<string>(),
                            UrlPhoto = item["urlPhoto"].Value<string>(),
                            RoleName = item["character"].Value<string>()
                        };

                        dict["Actor"].Add(p);
                    }

                    /*JArray writers = json["writers"].Value<JArray>();

                    foreach (var item in writers)
                    {
                        Dbo.Person p = new Dbo.Person()
                        {
                            Id = item["nameId"].Value<string>(),
                            Name = item["name"].Value<string>()
                         };
                    }*/

                    JArray directors = json["directors"].Value<JArray>();

                    foreach (var item in directors)
                    {
                        Dbo.Person p = new Dbo.Person()
                        {
                            Id = item["nameId"].Value<string>().Substring(2),
                            Name = item["name"].Value<string>()
                        };

                        dict["Director"].Add(p);
                    }


                }

            }

            return dict;
        }

        public static async Task<List<Dbo.Season>> GetSeasons(string serieId)
        {
            List<Dbo.Season> seasons = new List<Dbo.Season>();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://www.myapifilms.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("imdb?idIMDB=tt" + serieId + "&format=JSON&aka=0&business=0&seasons=1&seasonYear=0&technical=0&lang=en-us&actors=N&biography=0&trailer=0&uniqueName=0&filmography=0&bornDied=0&starSign=0&actorActress=0&actorTrivia=0&movieTrivia=0&awards=0&moviePhotos=N&movieVideos=N&similarMovies=0&token=" + Serie._token).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    string s = await response.Content.ReadAsStringAsync();
                    JObject json = JsonConvert.DeserializeObject<JObject>(s);

                    JArray jSeasons = json["seasons"].Value<JArray>();

                    foreach (var item in jSeasons)
                    {
                        JArray episodes = item["episodes"].Value<JArray>();
                        Dbo.Season season = new Dbo.Season()
                        {
                            IdSerie = serieId,
                            Number = item["numSeason"].Value<short>(),
                            Episodes = new List<Dbo.Episode>()
                        };

                        foreach (var episode in episodes)
                        {
                            Dbo.Episode ep = new Dbo.Episode()
                            {
                                Id = episode["idIMDB"].Value<string>().Substring(2),
                                Name = episode["title"].Value<string>(),
                                Synopsis = episode["plot"].Value<string>(),
                                UrlPhoto = episode["urlPoster"].Value<string>(),
                                Number = episode["episode"].Value<short>(),
                                ReleaseDate = (episode["date"].Value<string>() == "") ?
                                                    default(DateTime) : DateTime.Parse(episode["date"].Value<string>(),
                                                                        System.Globalization.CultureInfo.InvariantCulture)
                            };

                            season.Episodes.Add(ep);
                        }

                        seasons.Add(season);
                    }


                }

            }

            return seasons;
        }
    }
}