﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace CineCommunity.DataAccess.MyAPIFilms
{
    public class Film : ApiHandler
    {

        public static Dbo.Film GetSimpleData(JObject item)
        {
            JToken tempToken = null;

            item.TryGetValue("idIMDB", out tempToken);

            Dbo.Film f = new Dbo.Film();
            f.Id = ((JValue)tempToken).Value<string>().Substring(2);

            item.TryGetValue("title", out tempToken);
            f.Name = ((JValue)tempToken).Value<string>();

            item.TryGetValue("plot", out tempToken);
            f.Synopsis = ((JValue)tempToken).Value<string>();

            item.TryGetValue("urlPoster", out tempToken);
            f.UrlPhoto = ((JValue)tempToken).Value<string>();

            item.TryGetValue("releaseDate", out tempToken);
            string format = "yyyy";

            if (((JValue)tempToken).Value<string>().Length > 4)
                format += "MM";
            if (((JValue)tempToken).Value<string>().Length > 6)
                format += "dd";

            f.ReleaseDate = (((JValue)tempToken).Value<string>() == "") ?
                                   default(DateTime) : DateTime.ParseExact(((JValue)tempToken).Value<string>(),
                                                                           format,
                                                                           System.Globalization.CultureInfo.InvariantCulture);

            item.TryGetValue("genres", out tempToken);

            f.Types = new List<string>();
            foreach (var genre in (JArray)tempToken)
            {
                string genreValue = ((JValue)genre).Value<string>();
                f.Types.Add(genreValue);
            }

            item.TryGetValue("rating", out tempToken);

            f.Rating = (((JValue)tempToken).Value<string>().Length == 0) ? 0 : ((JValue)tempToken).Value<double>();

            return f;
        }

        public static async Task<Dbo.Film> GetFilmById(string filmId)
        {
            Dbo.Film f = new Dbo.Film();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://www.myapifilms.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("imdb?idIMDB=tt" + filmId + "&format=JSON&aka=0&business=0&seasons=0&seasonYear=0&technical=0&lang=en-us&actors=N&biography=0&trailer=0&uniqueName=0&filmography=0&bornDied=0&starSign=0&actorActress=0&actorTrivia=0&movieTrivia=0&awards=0&moviePhotos=N&movieVideos=N&similarMovies=0&token=" + Film._token).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    string s = await response.Content.ReadAsStringAsync();
                    JObject json = JsonConvert.DeserializeObject<JObject>(s);

                    f = GetSimpleData(json);
                }

            }

            return f;
        }

        public static async Task<Dictionary<String, List<Dbo.Person>>> GetDetailledInfo(string filmId)
        {
            Dictionary<String, List<Dbo.Person>> dict = new Dictionary<string, List<Dbo.Person>>() {
                { "Actor", new List<Dbo.Person>() },
                { "Director", new List<Dbo.Person>() }
            };

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://www.myapifilms.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("imdb?idIMDB=tt" + filmId + "&format=JSON&aka=0&business=0&seasons=0&seasonYear=0&technical=0&lang=en-us&actors=S&biography=0&trailer=0&uniqueName=0&filmography=0&bornDied=0&starSign=0&actorActress=0&actorTrivia=0&movieTrivia=0&awards=0&moviePhotos=N&movieVideos=N&similarMovies=0&token=" + Film._token).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    string s = await response.Content.ReadAsStringAsync();
                    JObject json = JsonConvert.DeserializeObject<JObject>(s);

                    JArray actors = json["actors"].Value<JArray>();

                    foreach (var item in actors)
                    {
                        Dbo.Person p = new Dbo.Person()
                        {
                            Id = item["actorId"].Value<string>().Substring(2),
                            Name = item["actorName"].Value<string>(),
                            UrlPhoto = item["urlPhoto"].Value<string>()
                        };

                        dict["Actor"].Add(p);
                    }

                    /*JArray writers = json["writers"].Value<JArray>();

                    foreach (var item in writers)
                    {
                        Dbo.Person p = new Dbo.Person()
                        {
                            Id = item["nameId"].Value<string>(),
                            Name = item["name"].Value<string>()
                         };
                    }*/

                    JArray directors = json["directors"].Value<JArray>();

                    foreach (var item in directors)
                    {
                        Dbo.Person p = new Dbo.Person()
                        {
                            Id = item["nameId"].Value<string>().Substring(2),
                            Name = item["name"].Value<string>()
                        };

                        dict["Director"].Add(p);
                    }


                }

            }

            return dict;
        }

        public static async Task<List<Dbo.Film>> GetTopFilms()
        {
            List<Dbo.Film> films = new List<Dbo.Film>();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://www.myapifilms.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("imdb/top?format=JSON&data=F&token=" + Film._token).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    string s = await response.Content.ReadAsStringAsync();
                    JArray json = JsonConvert.DeserializeObject<JArray>(s);

                    foreach (var item in json.Children<JObject>())
                    {
                        Dbo.Film f = MyAPIFilms.Film.GetSimpleData(item);

                        films.Add(f);

                    }

                }

            }

            return films;
        }

        public static async Task<Dictionary<string, List<Dbo.Film>>> GetFilmsInTheater()
        {
            Dictionary<string, List<Dbo.Film>> filmsDic = new Dictionary<string, List<Dbo.Film>>();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://www.myapifilms.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("imdb/inTheaters?format=JSON&lang=en-us&token=" + Film._token).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    string s = await response.Content.ReadAsStringAsync();
                    JArray json = JsonConvert.DeserializeObject<JArray>(s);

                    foreach (var item in json.Children<JObject>())
                    {
                        filmsDic.Add(item["date"].Value<string>(), new List<Dbo.Film>());
                        foreach (var film in item["movies"].Children<JObject>())
                        {
                            Dbo.Film f = MyAPIFilms.Film.GetSimpleData(film);
                            filmsDic[item["date"].Value<string>()].Add(f);
                        }

                    }

                }

            }

            return filmsDic;
        }

        public static async Task<Dictionary<string, List<Dbo.Film>>> GetFilmsCommingSoon()
        {
            Dictionary<string, List<Dbo.Film>> filmsDic = new Dictionary<string, List<Dbo.Film>>();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://www.myapifilms.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("imdb/comingSoon?format=JSON&lang=en-us&date=2015-07&token=" + Film._token).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    string s = await response.Content.ReadAsStringAsync();
                    JArray json = JsonConvert.DeserializeObject<JArray>(s);

                    foreach (var item in json.Children<JObject>())
                    {
                        filmsDic.Add(item["date"].Value<string>(), new List<Dbo.Film>());
                        foreach (var film in item["movies"].Children<JObject>())
                        {
                            Dbo.Film f = MyAPIFilms.Film.GetSimpleData(film);
                            filmsDic[item["date"].Value<string>()].Add(f);
                        }

                    }

                }

            }

            return filmsDic;
        }
    }
}