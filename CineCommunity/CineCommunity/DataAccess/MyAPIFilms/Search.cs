﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace CineCommunity.DataAccess.MyAPIFilms
{
    public class Search : ApiHandler
    {
        public static async Task<Tuple<List<Dbo.Film>, List<Dbo.Serie>>> SearchMedia(string searchInput)
        {
            List<Dbo.Film> films = null;
            List<Dbo.Serie> series = null;

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://www.myapifilms.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("imdb?title=" + searchInput + "&format=JSON&aka=0&business=0&seasons=0&seasonYear=0&technical=0&filter=N&exactFilter=0&limit=10&forceYear=0&lang=en-us&actors=N&biography=0&trailer=0&uniqueName=0&filmography=0&bornDied=0&starSign=0&actorActress=0&actorTrivia=0&movieTrivia=0&awards=0&moviePhotos=N&movieVideos=N&similarMovies=0&token=" + Search._token).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    string s = await response.Content.ReadAsStringAsync();
                    JArray json = (JArray)JsonConvert.DeserializeObject(s);

                    films = new List<Dbo.Film>();
                    series = new List<Dbo.Serie>();

                    foreach (JObject item in json.Children<JObject>())
                    {
                        JToken type = null;

                        if (item.TryGetValue("type", out type) && ((JValue)type).Value.ToString() == "TV Series")
                        {
                            Dbo.Serie serie = DataAccess.MyAPIFilms.Serie.GetSimpleData(item);

                            series.Add(serie);
                        }
                        else
                        {
                            Dbo.Film f = MyAPIFilms.Film.GetSimpleData(item);

                            films.Add(f);
                        }
                    }
                }

            }

            return new Tuple<List<Dbo.Film>, List<Dbo.Serie>>(films, series);
        }

        public static async Task<List<Dbo.Person>> SearchPerson(string searchInput)
        {
            List<Dbo.Person> people = null;

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://www.myapifilms.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("imdb?name=" + searchInput + "&format=JSON&filmography=0&limit=5&lang=en-us&exactFilter=0&bornDied=0&starSign=0&uniqueName=0&actorActress=0&actorTrivia=0&actorPhotos=N&actorVideos=N&salary=0&spouses=0&tradeMark=0&personalQuotes=0&token=" + Search._token).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    string s = await response.Content.ReadAsStringAsync();
                    JArray json = (JArray) JsonConvert.DeserializeObject(s);

                    people = new List<Dbo.Person>();

                    foreach (JObject item in json.Children<JObject>())
                    {
                        Dbo.Person p = MyAPIFilms.Person.GetSimpleData(item);

                        people.Add(p);
                    }
                }
                else
                    people = null;
            }

            return people;
        }
    }
}