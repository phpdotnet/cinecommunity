﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace CineCommunity.DataAccess.MyAPIFilms
{
    public class Episode : ApiHandler
    {
        public static Dbo.Episode GetSimpleData(JObject item)
        {
            JToken tempToken = null;

            item.TryGetValue("idIMDB", out tempToken);

            Dbo.Episode episode = new Dbo.Episode();
            episode.Id = ((JValue)tempToken).Value<string>().Substring(2);

            item.TryGetValue("title", out tempToken);
            episode.Name = ((JValue)tempToken).Value<string>();

            item.TryGetValue("plot", out tempToken);
            episode.Synopsis = ((JValue)tempToken).Value<string>();

            item.TryGetValue("urlPoster", out tempToken);
            episode.UrlPhoto = ((JValue)tempToken).Value<string>();

            item.TryGetValue("releaseDate", out tempToken);
            episode.ReleaseDate = (((JValue)tempToken).Value<string>() == "") ?
                                   default(DateTime) : DateTime.ParseExact(((JValue)tempToken).Value<string>(),
                                                                           "yyyyMMdd",
                                                                           System.Globalization.CultureInfo.InvariantCulture);

            item.TryGetValue("rating", out tempToken);

            episode.Rating = (((JValue)tempToken).Value<string>().Length == 0) ? 0 : ((JValue)tempToken).Value<double>();

            item.TryGetValue("seasonInfo", out tempToken);
            JObject tempObj = tempToken.Value<JObject>();

            tempObj.TryGetValue("episode", out tempToken);
            short episodeNumber = 0;
            short.TryParse((((JValue)tempToken).Value<string>().Substring(1).Split(' ')[1] != "")
                            ? ((JValue)tempToken).Value<string>().Substring(1).Split(' ')[1]
                            : "0",
                            out episodeNumber);
            episode.Number = episodeNumber;

            tempObj.TryGetValue("season", out tempToken);
            short seasonNumber = 0;
            short.TryParse((((JValue)tempToken).Value<string>().Substring(1).Split(' ')[1] != "")
                            ? ((JValue)tempToken).Value<string>().Substring(1).Split(' ')[1]
                            : "0",
                            out seasonNumber);

            tempObj.TryGetValue("tvSerie", out tempToken);


            episode.Season = new Dbo.Season() { IdSerie = tempToken["id"].Value<string>(),
                                                Number = seasonNumber };

            return episode;
        }

        public static async Task<Dbo.Episode> GetSerieById(string episodeId)
        {
            Dbo.Episode episode = new Dbo.Episode();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://www.myapifilms.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("imdb?idIMDB=tt" + episodeId + "&format=JSON&aka=0&business=0&seasons=0&seasonYear=0&technical=0&lang=en-us&actors=N&biography=0&trailer=0&uniqueName=0&filmography=0&bornDied=0&starSign=0&actorActress=0&actorTrivia=0&movieTrivia=0&awards=0&moviePhotos=N&movieVideos=N&similarMovies=0&token=" + Episode._token).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    string s = await response.Content.ReadAsStringAsync();
                    JObject json = JsonConvert.DeserializeObject<JObject>(s);

                    episode = GetSimpleData(json);
                }

            }

            return episode;
        }
    }
}