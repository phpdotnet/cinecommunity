﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace CineCommunity.DataAccess.MyAPIFilms
{
    public class Person : ApiHandler
    {

        public static Dbo.Person GetSimpleData(JObject item)
        {
            Dbo.Person p = new Dbo.Person();
            JToken tempToken = null;

            item.TryGetValue("idIMDB", out tempToken);
            p.Id = ((JValue)tempToken).Value<string>().Substring(2);

            item.TryGetValue("name", out tempToken);
            p.Name = ((JValue)tempToken).Value<string>();

            item.TryGetValue("bio", out tempToken);

            p.Biography = ((JValue)tempToken).Value<string>();

            item.TryGetValue("height", out tempToken);
            p.Height = ((JValue)tempToken).Value<string>();

            item.TryGetValue("dateOfBirth", out tempToken);
            p.BirthDate = (((JValue)tempToken).Value<string>() == "") ?
                                    default(DateTime) : DateTime.Parse(((JValue)tempToken).Value<string>(),
                                                                       System.Globalization.CultureInfo.InvariantCulture);
            item.TryGetValue("placeOfBirth", out tempToken);
            p.BirthPlace = ((JValue)tempToken).Value<string>();

            item.TryGetValue("urlPhoto", out tempToken);
            p.UrlPhoto = ((JValue)tempToken).Value<string>();

            return p;
        }

        public static async Task<Dictionary<String, Tuple<List<Dbo.Film>, List<Dbo.Serie>>>> GetFilmography(string personId)
        {
            Dictionary<String, Tuple<List<Dbo.Film>, List<Dbo.Serie>>> dict = new Dictionary<string, Tuple<List<Dbo.Film>, List<Dbo.Serie>>>() {
                { "Actor", new Tuple<List<Dbo.Film>, List<Dbo.Serie>>(new List<Dbo.Film>(), new List<Dbo.Serie>()) },
                { "Producer", new Tuple<List<Dbo.Film>, List<Dbo.Serie>>(new List<Dbo.Film>(), new List<Dbo.Serie>()) },
                { "Director", new Tuple<List<Dbo.Film>, List<Dbo.Serie>>(new List<Dbo.Film>(), new List<Dbo.Serie>()) }
            };

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://www.myapifilms.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("imdb?idName=nm" + personId + "&format=JSON&filmography=1&lang=en-us&bornDied=0&starSign=0&uniqueName=0&actorActress=0&actorTrivia=0&actorPhotos=N&actorVideos=N&salary=0&spouses=0&tradeMark=0&personalQuotes=0&token=" + Person._token).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    string s = await response.Content.ReadAsStringAsync();
                    JObject json = JsonConvert.DeserializeObject<JObject>(s);

                    JArray filmo = json["filmographies"].Value<JArray>();

                    foreach (var item in filmo)
                    {
                        if (item["section"] != null)
                        {
                            string section = item["section"].Value<string>();

                            if (dict.ContainsKey(section))
                            {
                                foreach (var media in item["filmography"].Value<JArray>())
                                {
                                    string id = media["IMDBId"].Value<string>().Substring(2);
                                    string name = media["title"].Value<string>();
                                    string year = media["year"].Value<string>();

                                    year = (year.Length > 1) ? year.Substring(1) : year;

                                    if (media["remarks"][0].Value<string>() == "(TV Series)"
                                        || media["remarks"][0].Value<string>() == "(TV Series short)")
                                    {
                                        if (media["remarks"].Value<JArray>().Count == 1
                                            || media["remarks"][1].Value<string>() != "(1 episode)")
                                        {
                                            Dbo.Serie serie = new Dbo.Serie() { Id = id, Name = name };
                                                
                                            if (year.Length > 1)
                                            {
                                                string[] years = year.Split('-');

                                                string end = (years.Length > 1 && years[1] != "") ? years[1] : null;
                                                serie.ReleaseDate = DateTime.ParseExact(years[0],
                                                                                        "yyyy",
                                                                                            System.Globalization.CultureInfo.InvariantCulture);

                                                if (end != null)
                                                    serie.EndDate = DateTime.ParseExact(end,
                                                                                        "yyyy",
                                                                                            System.Globalization.CultureInfo.InvariantCulture);

                                            }
                                                

                                            dict[section].Item2.Add(serie);
                                        }
                                           
                                    }
                                    else
                                    {
                                        Dbo.Film film = new Dbo.Film() { Id = id, Name = name };

                                        film.ReleaseDate = (year == " ") ?
                                                            default(DateTime) : DateTime.ParseExact(year,
                                                                        "yyyy",
                                                                        System.Globalization.CultureInfo.InvariantCulture);
                                        
                                        dict[section].Item1.Add(film);
                                    }
                                }
                            }          

                        }
                        
                    }
                }

            }

            return dict;
        }

        public static async Task<Dbo.Person> GetPersonById(string personId)
        {
            Dbo.Person p = new Dbo.Person();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://www.myapifilms.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("imdb?idName=nm" + personId + "&format=JSON&filmography=0&lang=en-us&bornDied=0&starSign=0&uniqueName=0&actorActress=0&actorTrivia=0&actorPhotos=N&actorVideos=N&salary=0&spouses=0&tradeMark=0&personalQuotes=0&token=" + Person._token).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    string s = await response.Content.ReadAsStringAsync();
                    JObject json = JsonConvert.DeserializeObject<JObject>(s);

                    p = GetSimpleData(json);
                }

            }

            return p;
        }
    }
}