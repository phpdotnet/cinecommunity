﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.DataAccess
{
    public class Episode
    {
        public List<Dbo.Episode> GetList(string idSeason)
        {
            List<Dbo.Episode> episodes = new List<Dbo.Episode>();
            using (var ctx = new CineCommunityEntities())
            {
                List<T_Episode> t_episodes = ctx.T_Media.OfType<T_Episode>().Where(x => x.id_season == idSeason).ToList();
                foreach (var t_episode in t_episodes)
                {
                    Dbo.Season season = new Season().GetId(t_episode.id_season);
                    Dbo.Episode episode = new Dbo.Episode()
                    {
                        Id = t_episode.id,
                        ReleaseDate = t_episode.release_date,
                        Season = season,
                        Name = t_episode.name,
                        Number = t_episode.number,
                        UrlPhoto = t_episode.url_photo,
                        Synopsis = t_episode.synopsis,
                        Rating = t_episode.rating
                    };
                    episodes.Add(episode);
                }
            }
            return episodes;
        }

        public Dbo.Episode GetId(string id)
        {
            Dbo.Episode episode = null;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Episode t_episode = ctx.T_Media.OfType<T_Episode>().Where(x => x.id == id).FirstOrDefault();
                    Dbo.Season season = new Season().GetId(t_episode.id_season);
                    episode = new Dbo.Episode()
                    {
                        Id = t_episode.id,
                        ReleaseDate = t_episode.release_date,
                        Season = season,
                        Name = t_episode.name,
                        Number = t_episode.number,
                        UrlPhoto = t_episode.url_photo,
                        Synopsis = t_episode.synopsis,
                        Rating = t_episode.rating
                    };
                }
                catch (Exception) { }
            }
            return episode;
        }

        public bool Create(Dbo.Episode episode)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                T_Episode t_episode = new T_Episode()
                {
                        id = episode.Id,
                        release_date = episode.ReleaseDate,
                        id_season = episode.Season.Id,
                        name = episode.Name,
                        number = episode.Number,
                        url_photo = episode.UrlPhoto,
                        synopsis = episode.Synopsis,
                        rating = episode.Rating
                };
                try
                {
                    ctx.T_Media.Add(t_episode);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Delete(string id)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Episode t_episode = ctx.T_Media.OfType<T_Episode>().Where(x => x.id == id).SingleOrDefault();
                    ctx.T_Media.Remove(t_episode);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Update(Dbo.Episode episode)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Episode t_episode = ctx.T_Media.OfType<T_Episode>().Where(x => x.id == episode.Id).SingleOrDefault();
                    t_episode.name = episode.Name;
                    t_episode.number = episode.Number;
                    t_episode.url_photo = episode.UrlPhoto;
                    t_episode.synopsis = episode.Synopsis;
                    t_episode.release_date = episode.ReleaseDate;
                    t_episode.rating = episode.Rating;
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

    }
}