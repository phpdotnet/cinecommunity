﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.DataAccess
{
    public class RealiseMedia
    {
        public bool Create(Dbo.RealiseMedia realise_media)
        {
            T_Realise_Media t_realise_media = new T_Realise_Media()
            {
                id = realise_media.Id,
                id_person = realise_media.IdPerson,
                id_media = realise_media.IdMedia,
            };
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    ctx.T_Realise_Media.Add(t_realise_media);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Update(Dbo.RealiseMedia realise_media)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Realise_Media t_realise_media = ctx.T_Realise_Media.Where(x => x.id == realise_media.Id).SingleOrDefault();
                    t_realise_media.id_person = realise_media.IdPerson;
                    t_realise_media.id_media = realise_media.IdMedia;

                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }
    }
}