﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.DataAccess
{
    public class Follow
    {
        public bool Create(Dbo.Follow follow)
        {
            T_Follow t_follow = new T_Follow()
            {
                id = follow.Id,
                id_follower = follow.IdFollower,
                id_following = follow.IdFollowing,
                is_following_person = follow.IsFollowingPerson
            };
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    ctx.T_Follow.Add(t_follow);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public int GetFollowersCount(string id_following, bool is_person)
        {
            int count = 0;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    count = ctx.T_Follow.Where(x => x.id_following == id_following && x.is_following_person == is_person).ToList().Count;
                }
                catch (Exception) { }
            }
            return count;
        }

        public Dbo.Follow GetId(long id)
        {
            Dbo.Follow follow = null;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Follow t_follow = ctx.T_Follow.Where(x => x.id == id).FirstOrDefault();
                    follow = new Dbo.Follow()
                    {
                        Id = t_follow.id,
                        IdFollower = t_follow.id_follower,
                        IdFollowing = t_follow.id_following,
                        IsFollowingPerson = t_follow.is_following_person
                    };
                }
                catch (Exception) { }
            }
            return follow;
        }

        public Dbo.Follow GetFollow(Dbo.Follow tmp_follow)
        {
            Dbo.Follow follow = null;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Follow t_follow = ctx.T_Follow
                        .Where(x => x.id_follower == tmp_follow.IdFollower && x.id_following == tmp_follow.IdFollowing)
                        .FirstOrDefault();
                    follow = new Dbo.Follow()
                    {
                        Id = t_follow.id,
                        IdFollower = t_follow.id_follower,
                        IdFollowing = t_follow.id_following,
                        IsFollowingPerson = t_follow.is_following_person
                    };
                }
                catch (Exception) { }
            }
            return follow;
        }

        public bool Delete(long id)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Follow t_follow = ctx.T_Follow.Where(x => x.id == id).SingleOrDefault();
                    ctx.T_Follow.Remove(t_follow);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Update(Dbo.Follow follow)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Follow t_follow = ctx.T_Follow.Where(x => x.id == follow.Id).SingleOrDefault();
                    t_follow.id_follower = follow.IdFollower;
                    t_follow.id_following = follow.IdFollowing;
                    t_follow.is_following_person = follow.IsFollowingPerson;

                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }
    }
}