﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.DataAccess
{
    public class Person
    {
        public List<Dbo.Person> GetList()
        {
            List<Dbo.Person> persons = new List<Dbo.Person>();
            using (var ctx = new CineCommunityEntities())
            {
                List<T_Person> t_persons = ctx.T_Person.ToList();
                foreach (var t_person in t_persons)
                {
                    Dbo.Person person = new Dbo.Person()
                    {
                        Id = t_person.id,
                        Biography = t_person.biography,
                        Name = t_person.name,
                        Height = t_person.height,
                        BirthDate = t_person.birth_date,
                        BirthPlace = t_person.birth_place,
                        UrlPhoto = t_person.url_photo
                    };
                    persons.Add(person);
                }
            }
            return persons;
        }

        //Tuple: Role, Film
        public List<Tuple<string, Dbo.Film>> GetFilmsAct(string id)
        {
            List<Tuple<string, Dbo.Film>> films = new List<Tuple<string, Dbo.Film>>();
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    List<T_Act_Media> t_acts = ctx.T_Act_Media.Where(x => x.id_person == id).ToList();
                    foreach (var t_act in t_acts)
                    {
                        List<String> types = ctx.T_Type
                            .Join(ctx.T_Is_Type,
                                    t => t.id,
                                    ti => ti.id_type,
                                    (t, ti) => new { name = t.name, id_media = ti.id_media })
                            .Where(x => x.id_media == t_act.id_media).Select(x => x.name).ToList();
                        T_Film t_film = ctx.T_Media.OfType<T_Film>().Where(x => x.id == t_act.id_media).FirstOrDefault();
                        if (t_film != null)
                        {
                            Dbo.Film film = new Dbo.Film()
                            {
                                Id = t_film.id,
                                ReleaseDate = t_film.release_date,
                                Name = t_film.name,
                                UrlPhoto = t_film.url_photo,
                                Synopsis = t_film.synopsis,
                                Rating = t_film.rating,
                                Types = types
                            };
                            films.Add(new Tuple<string, Dbo.Film>(t_act.role, film));
                        }
                    }
                }
                catch (Exception) { }
            }
            return films;
        }

        //Tuple: Role, Serie
        public List<Tuple<string, Dbo.Serie>> GetSeriesAct(string id)
        {

            List<Tuple<string, Dbo.Serie>> series = new List<Tuple<string, Dbo.Serie>>();
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    List<T_Act_Media> t_acts = ctx.T_Act_Media.Where(x => x.id_person == id).ToList();
                    foreach (var t_act in t_acts)
                    {
                        List<String> types = ctx.T_Type
                            .Join(ctx.T_Is_Type,
                                    t => t.id,
                                    ti => ti.id_type,
                                    (t, ti) => new { name = t.name, id_media = ti.id_media })
                            .Where(x => x.id_media == t_act.id_media).Select(x => x.name).ToList();
                        T_Serie t_serie = ctx.T_Media.OfType<T_Serie>().Where(x => x.id == t_act.id_media).FirstOrDefault();
                        if (t_serie != null)
                        {
                            Dbo.Serie serie = new Dbo.Serie()
                            {
                                Id = t_serie.id,
                                ReleaseDate = t_serie.release_date,
                                EndDate = t_serie.end_date,
                                Name = t_serie.name,
                                UrlPhoto = t_serie.url_photo,
                                Rating = t_serie.rating,
                                Synopsis = t_serie.synopsis,
                                Types = types
                            };
                            series.Add(new Tuple<string, Dbo.Serie>(t_act.role, serie));
                        }
                    }
                }
                catch (Exception) { }
            }
            return series;
        }

        public Dbo.Person GetId(string id)
        {
            Dbo.Person person = null;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Person t_person = ctx.T_Person.Where(x => x.id == id).FirstOrDefault();
                    person = new Dbo.Person()
                    {
                        Id = t_person.id,
                        Biography = t_person.biography,
                        Name = t_person.name,        
                        Height = t_person.height,
                        BirthDate = t_person.birth_date,
                        BirthPlace = t_person.birth_place,
                        UrlPhoto = t_person.url_photo
                    };
                }
                catch (Exception) { }
            }
            return person;
        }

        public bool Create(Dbo.Person person)
        {
            T_Person t_person = new T_Person()
            {
                id = person.Id,
                name = person.Name,
                biography = person.Biography,
                height = person.Height,
                birth_date = person.BirthDate,
                birth_place = person.BirthPlace,
                url_photo = person.UrlPhoto
            };
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    ctx.T_Person.Add(t_person);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Delete(string id)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Person t_person = ctx.T_Person.Where(x => x.id == id).SingleOrDefault();
                    ctx.T_Person.Remove(t_person);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Update(Dbo.Person person)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Person t_person = ctx.T_Person.Where(x => x.id == person.Id).SingleOrDefault();
                    t_person.name = person.Name;
                    t_person.biography = person.Biography;

                    t_person.height = person.Height;
                    t_person.birth_date = person.BirthDate;
                    t_person.birth_place = person.BirthPlace;
                    t_person.url_photo = person.UrlPhoto;

                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }
    }
}