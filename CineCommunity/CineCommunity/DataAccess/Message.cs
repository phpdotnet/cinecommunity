﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.DataAccess
{
    public class Message
    {
        public List<Dbo.Message> GetList(long idSubject)
        {
            List<Dbo.Message> messages = new List<Dbo.Message>();
            using (var ctx = new CineCommunityEntities())
            {
                var t_messages = ctx.T_Message
                    .Join(ctx.T_User, m => m.id_user, u => u.id, (m, u) => new { id = m.id, id_subject = m.id_subject, username = u.username, text = m.text})
                    .Where(x => x.id_subject == idSubject).ToList();
                foreach (var t_message in t_messages)
                {
                    Dbo.Message message = new Dbo.Message()
                    {
                        id = t_message.id,
                        id_subject = t_message.id_subject,
                        username = t_message.username,
                        text = t_message.text
                    };
                    messages.Add(message);
                }
            }
            return messages;
        }

        public Dbo.Message GetId(long id)
        {
            Dbo.Message message = null;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                     var t_message = ctx.T_Message
                    .Join(ctx.T_User, m => m.id_user, u => u.id, (m, u) => new { id = m.id, id_subject = m.id_subject, username = u.username, text = m.text})
                    .Where(x => x.id == id).FirstOrDefault();
                    message = new Dbo.Message()
                    {
                        id = t_message.id,
                        id_subject = t_message.id_subject,
                        username = t_message.username,
                        text = t_message.text
                    };
                }
                catch (Exception) { }
            }
            return message;
        }

        public bool Create(Dbo.Message message)
        {
            T_Message t_message = new T_Message()
            {
                id_subject = message.id_subject,
                id_user = message.id_user,
                text = message.text
            };
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    ctx.T_Message.Add(t_message);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Delete(long id)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Message t_message = ctx.T_Message.Where(x => x.id == id).SingleOrDefault();
                    ctx.T_Message.Remove(t_message);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Update(Dbo.Message message)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Message t_message = ctx.T_Message.Where(x => x.id == message.id).SingleOrDefault();
                    t_message.id_subject = message.id_subject;
                    t_message.id_user = message.id_user;
                    t_message.text = message.text;
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }
    }
}