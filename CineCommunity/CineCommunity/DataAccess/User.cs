﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.DataAccess
{
    public class User
    {
        public List<Dbo.User> GetList()
        {
            List<Dbo.User> users = new List<Dbo.User>();
            using (var ctx = new CineCommunityEntities())
            {
                List<T_User> t_users = ctx.T_User.ToList();
                foreach (var t_user in t_users)
                {
                    Dbo.User user = new Dbo.User()
                    {
                        id = t_user.id,
                        firstname = t_user.firstname,
                        lastname = t_user.lastname,
                        gender = t_user.gender,
                        birth_date = t_user.birth_date,
                        photo = t_user.photo,
                        username = t_user.username,
                        password = t_user.password,
                        email = t_user.email
                    };
                    users.Add(user);
                }
            }
            return users;
        }

        public Dbo.User GetId(long id)
        {
            Dbo.User user = null;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_User t_user = ctx.T_User.Where(x => x.id == id).FirstOrDefault();
                    user = new Dbo.User()
                    {
                        id = t_user.id,
                        firstname = t_user.firstname,
                        lastname = t_user.lastname,
                        gender = t_user.gender,
                        birth_date = t_user.birth_date,
                        photo = t_user.photo,
                        username = t_user.username,
                        password = t_user.password,
                        email = t_user.email
                    };
                }
                catch (Exception) { }
            }
            return user;
        }

        public Dbo.User GetByUsername(string username)
        {
            Dbo.User user = null;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_User t_user = ctx.T_User.Where(x => x.username == username).FirstOrDefault();
                    user = new Dbo.User()
                    {
                        id = t_user.id,
                        firstname = t_user.firstname,
                        lastname = t_user.lastname,
                        gender = t_user.gender,
                        birth_date = t_user.birth_date,
                        photo = t_user.photo,
                        username = t_user.username,
                        password = t_user.password,
                        email = t_user.email
                    };
                }
                catch (Exception) { }
            }
            return user;
        }

        public bool Create(Dbo.User user)
        {
            T_User t_user = new T_User()
            {
                firstname = user.firstname,
                lastname = user.lastname,
                gender = user.gender,
                birth_date = user.birth_date,
                photo = user.photo,
                username = user.username,
                password = user.password,
                email = user.email
            };
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    ctx.T_User.Add(t_user);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Delete(long id)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_User t_user = ctx.T_User.Where(x => x.id == id).SingleOrDefault();
                    ctx.T_User.Remove(t_user);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Update(Dbo.User user)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_User t_user = ctx.T_User.Where(x => x.id == user.id).SingleOrDefault();
                    t_user.lastname = user.lastname;
                    t_user.firstname = user.firstname;
                    t_user.photo = user.photo;
                    t_user.birth_date = user.birth_date;
                    t_user.gender = user.gender;
                    t_user.password = user.password;
                    t_user.email = user.email;
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

    }
}