﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.DataAccess
{
    public class ActMedia
    {
        public bool Create(Dbo.ActMedia act_media)
        {
            T_Act_Media t_act_media = new T_Act_Media()
            {
                id = act_media.Id,
                id_person = act_media.IdPerson,
                id_media = act_media.IdMedia,
                role = act_media.Role
            };
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    ctx.T_Act_Media.Add(t_act_media);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Update(Dbo.ActMedia act_media)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Act_Media t_act_media = ctx.T_Act_Media.Where(x => x.id == act_media.Id).SingleOrDefault();
                    t_act_media.id_person = act_media.IdPerson;
                    t_act_media.id_media = act_media.IdMedia;
                    t_act_media.role = act_media.Role;

                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }
    }
}