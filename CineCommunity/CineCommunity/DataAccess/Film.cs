﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.DataAccess
{
    public class Film
    {
        public List<Dbo.Film> GetList()
        {
            List<Dbo.Film> films = new List<Dbo.Film>();
            using (var ctx = new CineCommunityEntities())
            {
                List<T_Film> t_films = ctx.T_Media.OfType<T_Film>().ToList();
                foreach (var t_film in t_films)
                {
                    List<String> types = ctx.T_Type
                        .Join(ctx.T_Is_Type,
                                t => t.id,
                                ti => ti.id_type,
                                (t, ti) => new { name = t.name, id_media = ti.id_media })
                        .Where(x => x.id_media == t_film.id).Select(x => x.name).ToList();
                    Dbo.Film film = new Dbo.Film()
                    {
                        Id = t_film.id,
                        ReleaseDate = t_film.release_date,
                        Name = t_film.name,
                        UrlPhoto = t_film.url_photo,
                        Synopsis = t_film.synopsis,
                        Rating = t_film.rating,
                        Types = types
                    };
                    films.Add(film);
                }
            }
            return films;
        }

        public List<Dbo.Film> Search(string name)
        {
            List<Dbo.Film> films = new List<Dbo.Film>();
            using (var ctx = new CineCommunityEntities())
            {
                List<T_Film> t_films = ctx.T_Media.OfType<T_Film>().Where(x => x.name.Contains(name)).ToList();
                foreach (var t_film in t_films)
                {
                    List<String> types = ctx.T_Type
                        .Join(ctx.T_Is_Type,
                                t => t.id,
                                ti => ti.id_type,
                                (t, ti) => new { name = t.name, id_media = ti.id_media })
                        .Where(x => x.id_media == t_film.id).Select(x => x.name).ToList();
                    Dbo.Film film = new Dbo.Film()
                    {
                        Id = t_film.id,
                        ReleaseDate = t_film.release_date,
                        Name = t_film.name,
                        UrlPhoto = t_film.url_photo,
                        Synopsis = t_film.synopsis,
                        Rating = t_film.rating,
                        Types = types
                    };
                    films.Add(film);
                }
            }
            return films;
        }

        public List<Dbo.Film> GetListByType(short idType)
        {
            List<Dbo.Film> films = new List<Dbo.Film>();
            using (var ctx = new CineCommunityEntities())
            {
                var t_films = ctx.T_Media.OfType<T_Film>().Join(ctx.T_Is_Type, f => f.id, t => t.id_media, (f, t) => new
                {
                    id = f.id,
                    releaseDate = f.release_date,
                    name = f.name,
                    urlPhoto = f.url_photo,
                    synopsis = f.synopsis,
                    rating = f.rating,
                    id_type = t.id_type
                }).Where(x => x.id_type == idType).ToList();
                foreach (var t_film in t_films)
                {
                    List<String> types = ctx.T_Type
                        .Join(ctx.T_Is_Type,
                                t => t.id,
                                ti => ti.id_type,
                                (t, ti) => new { name = t.name, id_media = ti.id_media })
                        .Where(x => x.id_media == t_film.id).Select(x => x.name).ToList();
                    Dbo.Film film = new Dbo.Film()
                    {
                        Id = t_film.id,
                        ReleaseDate = t_film.releaseDate,
                        Name = t_film.name,
                        UrlPhoto = t_film.urlPhoto,
                        Synopsis = t_film.synopsis,
                        Rating = t_film.rating,
                        Types = types
                    };
                    films.Add(film);
                }
            }
            return films;
        }

        public Dbo.Film GetId(string id)
        {
            Dbo.Film film = null;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Film t_film = ctx.T_Media.OfType<T_Film>().Where(x => x.id == id).FirstOrDefault();
                    List<String> types = ctx.T_Type
                        .Join(ctx.T_Is_Type,
                                t => t.id,
                                ti => ti.id_type,
                                (t, ti) => new { name = t.name, id_media = ti.id_media })
                        .Where(x => x.id_media == t_film.id).Select(x => x.name).ToList();
                    film = new Dbo.Film()
                    {
                        Id = t_film.id,
                        ReleaseDate = t_film.release_date,
                        Name = t_film.name,
                        UrlPhoto = t_film.url_photo,
                        Synopsis = t_film.synopsis,
                        Rating = t_film.rating,
                        Types = types
                    };
                }
                catch (Exception) { }
            }
            return film;
        }

        public bool Create(Dbo.Film film)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                T_Film t_film = new T_Film()
                {
                    id = film.Id,
                    release_date = film.ReleaseDate,
                    name = film.Name,
                    url_photo = film.UrlPhoto,
                    rating = film.Rating,
                    synopsis = film.Synopsis
                };
                try
                {
                    ctx.T_Media.Add(t_film);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Delete(string id)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Film t_film = ctx.T_Media.OfType<T_Film>().Where(x => x.id == id).SingleOrDefault();
                    ctx.T_Media.Remove(t_film);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Update(Dbo.Film film)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Film t_film = ctx.T_Media.OfType<T_Film>().Where(x => x.id == film.Id).SingleOrDefault();
                    t_film.name = film.Name;
                    t_film.url_photo = film.UrlPhoto;
                    t_film.synopsis = film.Synopsis;
                    t_film.release_date = film.ReleaseDate;
                    t_film.rating = film.Rating;

                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }
    }
}