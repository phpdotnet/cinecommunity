﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.DataAccess
{
    public class Season
    {
        public List<Dbo.Season> GetList(string idSerie)
        {
            List<Dbo.Season> seasons = new List<Dbo.Season>();
            using (var ctx = new CineCommunityEntities())
            {
                List<T_Season> t_seasons = ctx.T_Media.OfType<T_Season>().Where(x => x.id_serie == idSerie).ToList();
                foreach (var t_season in t_seasons)
                {
                    List<Dbo.Episode> episodes = new Episode().GetList(t_season.id);
                    Dbo.Season season = new Dbo.Season()
                    {
                        Id = t_season.id,
                        Number = t_season.number,
                        IdSerie = t_season.id_serie,
                        Episodes = episodes
                    };
                    seasons.Add(season);
                }
            }
            return seasons;
        }

        public Dbo.Season GetId(string id)
        {
            Dbo.Season season = null;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Season t_season = ctx.T_Media.OfType<T_Season>().Where(x => x.id == id).FirstOrDefault();
                    List<Dbo.Episode> episodes = new Episode().GetList(id);
                    season = new Dbo.Season()
                    {
                        Id = t_season.id,
                        Number = t_season.number,
                        IdSerie = t_season.id_serie,
                        Episodes = episodes
                    };
                }
                catch (Exception) { }
            }
            return season;
        }

        public bool Create(Dbo.Season season)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                T_Season t_season = new T_Season()
                {
                    id = season.Id,
                    id_serie = season.IdSerie,
                    number = season.Number
                };
                try
                {
                    ctx.T_Media.Add(t_season);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Delete(string id)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Season t_season = ctx.T_Media.OfType<T_Season>().Where(x => x.id == id).SingleOrDefault();
                    ctx.T_Media.Remove(t_season);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Update(Dbo.Season season)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Season t_season = ctx.T_Media.OfType<T_Season>().Where(x => x.id == season.Id).SingleOrDefault();
                    t_season.number = season.Number;
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }
    }
}