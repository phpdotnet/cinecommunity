﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.DataAccess
{
    public class Type
    {
        public List<Dbo.Type> GetList()
        {
            List<Dbo.Type> types = new List<Dbo.Type>();
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    List<T_Type> t_types = ctx.T_Type.ToList();
                    foreach (var t_type in t_types)
                    {
                        List<Dbo.Film> films = new Film().GetListByType(t_type.id);
                        List<Dbo.Serie> series = new Serie().GetListByType(t_type.id);
                        Dbo.Type type = new Dbo.Type()
                        {
                            id = t_type.id,
                            name = t_type.name,
                            films = films,
                            series = series
                        };
                        types.Add(type);
                    }
                }
                catch (Exception) { }
            }
            return types;
        }

        public Dbo.Type GetId(short id)
        {
            Dbo.Type type = null;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Type t_type = ctx.T_Type.Where(x => x.id == id).FirstOrDefault();
                    List<Dbo.Film> films = new Film().GetListByType(t_type.id);
                    List<Dbo.Serie> series = new Serie().GetListByType(t_type.id);
                    type = new Dbo.Type()
                    {
                        id = t_type.id,
                        name = t_type.name,
                        films = films,
                        series = series
                    };
                }
                catch (Exception) { }
            }
            return type;
        }

        public bool Create(Dbo.Type type)
        {
            T_Type t_type = new T_Type()
            {
                name = type.name
            };
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    ctx.T_Type.Add(t_type);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Delete(short id)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Type t_type = ctx.T_Type.Where(x => x.id == id).SingleOrDefault();
                    ctx.T_Type.Remove(t_type);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Update(Dbo.Type type)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Type t_type = ctx.T_Type.Where(x => x.id == type.id).SingleOrDefault();
                    t_type.name = type.name;
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }
    }
}