﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.DataAccess
{
    public class Serie
    {
        public List<Dbo.Serie> GetList()
        {
            List<Dbo.Serie> series = new List<Dbo.Serie>();
            using (var ctx = new CineCommunityEntities())
            {
                List<T_Serie> t_series = ctx.T_Media.OfType<T_Serie>().ToList();
                foreach (var t_serie in t_series)
                {
                    List<String> types = ctx.T_Type
                        .Join(ctx.T_Is_Type,
                            t => t.id,
                            ti => ti.id_type,
                            (t, ti) => new { name = t.name, id_media = ti.id_media })
                        .Where(x => x.id_media == t_serie.id).Select(x => x.name).ToList();
                    Dbo.Serie serie = new Dbo.Serie()
                    {
                        Id = t_serie.id,
                        ReleaseDate = t_serie.release_date,
                        EndDate = t_serie.end_date,
                        Name = t_serie.name,
                        UrlPhoto = t_serie.url_photo,
                        Rating = t_serie.rating,
                        Synopsis = t_serie.synopsis,
                        Types = types
                    };
                    series.Add(serie);
                }
            }
            return series;
        }

        public List<Dbo.Serie> Search(string name)
        {
            List<Dbo.Serie> series = new List<Dbo.Serie>();
            using (var ctx = new CineCommunityEntities())
            {
                List<T_Serie> t_series = ctx.T_Media.OfType<T_Serie>().Where(x => x.name.Contains(name)).ToList();
                foreach (var t_serie in t_series)
                {
                    List<String> types = ctx.T_Type
                        .Join(ctx.T_Is_Type,
                            t => t.id,
                            ti => ti.id_type,
                            (t, ti) => new { name = t.name, id_media = ti.id_media })
                        .Where(x => x.id_media == t_serie.id).Select(x => x.name).ToList();
                    Dbo.Serie serie = new Dbo.Serie()
                    {
                        Id = t_serie.id,
                        ReleaseDate = t_serie.release_date,
                        EndDate = t_serie.end_date,
                        Name = t_serie.name,
                        UrlPhoto = t_serie.url_photo,
                        Rating = t_serie.rating,
                        Synopsis = t_serie.synopsis,
                        Types = types
                    };
                    series.Add(serie);
                }
            }
            return series;
        }

        public List<Dbo.Serie> GetListByType(short idType)
        {
            List<Dbo.Serie> series = new List<Dbo.Serie>();
            using (var ctx = new CineCommunityEntities())
            {
                var t_series = ctx.T_Media.OfType<T_Serie>().Join(ctx.T_Is_Type, f => f.id, t => t.id_media, (f, t) => new
                {
                    id = f.id,
                    releaseDate = f.release_date,
                    end_date = f.end_date,
                    name = f.name,
                    urlPhoto = f.url_photo,
                    synopsis = f.synopsis,
                    rating = f.rating,
                    id_type = t.id_type
                }).Where(x => x.id_type == idType).ToList();
                foreach (var t_serie in t_series)
                {
                    List<String> types = ctx.T_Type
                        .Join(ctx.T_Is_Type,
                                t => t.id,
                                ti => ti.id_type,
                                (t, ti) => new { name = t.name, id_media = ti.id_media })
                        .Where(x => x.id_media == t_serie.id).Select(x => x.name).ToList();
                    List<Dbo.Season> seasons = new Season().GetList(t_serie.id);
                    Dbo.Serie serie = new Dbo.Serie()
                    {
                        Id = t_serie.id,
                        ReleaseDate = t_serie.releaseDate,
                        EndDate = t_serie.end_date,
                        Name = t_serie.name,
                        UrlPhoto =  t_serie.urlPhoto,
                        Synopsis = t_serie.synopsis,
                        Rating = t_serie.rating,
                        Types = types,
                        Seasons = seasons
                    };
                    series.Add(serie);
                }
            }
            return series;
        }

        public Dbo.Serie GetId(string id)
        {
            Dbo.Serie serie = null;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Serie t_serie = ctx.T_Media.OfType<T_Serie>().Where(x => x.id == id).FirstOrDefault();
                    List<String> types = ctx.T_Type
                        .Join(ctx.T_Is_Type,
                                t => t.id,
                                ti => ti.id_type,
                                (t, ti) => new { name = t.name, id_media = ti.id_media })
                        .Where(x => x.id_media == t_serie.id).Select(x => x.name).ToList();
                    List<Dbo.Season> seasons = new Season().GetList(t_serie.id);
                    serie = new Dbo.Serie()
                    {
                        Id = t_serie.id,
                        ReleaseDate = t_serie.release_date,
                        EndDate = t_serie.end_date,
                        Name = t_serie.name,
                        UrlPhoto = t_serie.url_photo,
                        Synopsis = t_serie.synopsis,
                        Rating = t_serie.rating,
                        Types = types,
                        Seasons = seasons
                    };
                }
                catch (Exception) { }
            }
            return serie;
        }

        public bool Create(Dbo.Serie serie)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                T_Serie t_serie = new T_Serie()
                {
                    id = serie.Id,
                    release_date = serie.ReleaseDate,
                    end_date = serie.EndDate, 
                    name = serie.Name,
                    url_photo = serie.UrlPhoto,
                    rating = serie.Rating,
                    synopsis = serie.Synopsis
                };
                try
                {
                    ctx.T_Media.Add(t_serie);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Delete(string id)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Serie t_serie = ctx.T_Media.OfType<T_Serie>().Where(x => x.id == id).SingleOrDefault();
                    ctx.T_Media.Remove(t_serie);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Update(Dbo.Serie serie)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Serie t_serie = ctx.T_Media.OfType<T_Serie>().Where(x => x.id == serie.Id).SingleOrDefault();
                    t_serie.name = serie.Name;
                    t_serie.url_photo = serie.UrlPhoto;
                    t_serie.synopsis = serie.Synopsis;
                    t_serie.release_date = serie.ReleaseDate;
                    t_serie.end_date = serie.EndDate;
                    t_serie.rating = serie.Rating;
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }
    }
}