﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.DataAccess
{
    public class Subject
    {
        public List<Dbo.Subject> GetList()
        {
            List<Dbo.Subject> subjects = new List<Dbo.Subject>();
            using (var ctx = new CineCommunityEntities())
            {
                List<T_Subject> t_subjects = ctx.T_Subject.ToList();
                foreach (var t_subject in t_subjects)
                {
                    List<Dbo.Message> messages = new Message().GetList(t_subject.id);
                    Dbo.Subject subject = new Dbo.Subject()
                    {
                        id = t_subject.id,
                        create_date = t_subject.create_date,
                        title = t_subject.title,
                        description = t_subject.description,
                        messages = messages
                    };
                    subjects.Add(subject);
                }
            }
            return subjects;
        }

        public List<Dbo.Subject> GetList(int count)
        {
            List<Dbo.Subject> subjects = new List<Dbo.Subject>();
            using (var ctx = new CineCommunityEntities())
            {
                List<T_Subject> t_subjects = ctx.T_Subject.OrderByDescending(x => x.id).Take(count).ToList();
                foreach (var t_subject in t_subjects)
                {
                    List<Dbo.Message> messages = new Message().GetList(t_subject.id);
                    Dbo.Subject subject = new Dbo.Subject()
                    {
                        id = t_subject.id,
                        create_date = t_subject.create_date,
                        title = t_subject.title,
                        description = t_subject.description,
                        messages = messages
                    };
                    subjects.Add(subject);
                }
            }
            return subjects;
        }

        public Dbo.Subject GetId(long id)
        {
            Dbo.Subject subject = null;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Subject t_subject = ctx.T_Subject.Where(x => x.id == id).FirstOrDefault();
                    List<Dbo.Message> messages = new Message().GetList(t_subject.id);
                    subject = new Dbo.Subject()
                    {
                        id = t_subject.id,
                        create_date = t_subject.create_date,
                        title = t_subject.title,
                        description = t_subject.description,
                        messages = messages
                    };
                }
                catch (Exception) { }
            }
            return subject;
        }

        public Dbo.Subject GetByTitle(string title)
        {
            Dbo.Subject subject = null;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Subject t_subject = ctx.T_Subject.Where(x => x.title == title).FirstOrDefault();
                    List<Dbo.Message> messages = new Message().GetList(t_subject.id);
                    subject = new Dbo.Subject()
                    {
                        id = t_subject.id,
                        create_date = t_subject.create_date,
                        title = t_subject.title,
                        description = t_subject.description,
                        messages = messages
                    };
                }
                catch (Exception) { }
            }
            return subject;
        }

        public bool Create(Dbo.Subject subject)
        {
            T_Subject t_subject = new T_Subject()
            {
                title = subject.title,
                create_date = subject.create_date,
                description = subject.description
            };
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    ctx.T_Subject.Add(t_subject);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Delete(long id)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Subject t_subject = ctx.T_Subject.Where(x => x.id == id).SingleOrDefault();
                    ctx.T_Subject.Remove(t_subject);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }

        public bool Update(Dbo.Subject subject)
        {
            bool success = true;
            using (var ctx = new CineCommunityEntities())
            {
                try
                {
                    T_Subject t_subject = ctx.T_Subject.Where(x => x.id == subject.id).SingleOrDefault();
                    t_subject.create_date = subject.create_date;
                    t_subject.description = subject.description;
                    t_subject.title = subject.title;
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    success = false;
                }
            }
            return success;
        }
    }
}