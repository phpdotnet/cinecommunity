﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.BusinessManagement
{
    public class Message
    {
        private static DataAccess.Message dataAccess = new DataAccess.Message();

        public static List<Dbo.Message> GetList(int idSubject)
        {
            return dataAccess.GetList(idSubject);
        }

        public static Dbo.Message GetId(long id)
        {
            return dataAccess.GetId(id);
        }

        public static bool Create(Dbo.Message message)
        {
            return dataAccess.Create(message);
        }

        public static bool Update(Dbo.Message message)
        {
            return dataAccess.Update(message);
        }

        public static bool Delete(long id)
        {
            return dataAccess.Delete(id);
        }
    }
}