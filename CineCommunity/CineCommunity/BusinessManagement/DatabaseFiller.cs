﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.BusinessManagement
{
    public class DatabaseFiller
    {
        private static DataAccess.Episode dataAccess = new DataAccess.Episode();

        // Ajoute film ET actors
        static public void FilmFill(Dbo.Film film)
        {
            BusinessManagement.Film.Create(film);
            FilmFillActors(film.Id);
        }
        static public void SerieFill(Dbo.Serie serie)
        {
            BusinessManagement.Serie.Create(serie);
            SerieFillActors(serie.Id);
        }
        static public void PersonFill(Dbo.Person person)
        {
            BusinessManagement.Person.Create(person);
            PersonFillFilmography(person.Id);
        }

        static public void PersonFillFilmography(string id)
        {
            var list = DataAccess.MyAPIFilms.Person.GetFilmography(id).Result;
            foreach (Dbo.Film film in list["Actor"].Item1)
            {
                BusinessManagement.Film.Create(film);
                BusinessManagement.ActMedia.Create(new Dbo.ActMedia() {
                    IdMedia = film.Id,
                    IdPerson = id,
                    Role = ""
                });
            }
            foreach (Dbo.Serie serie in list["Actor"].Item2)
            {
                BusinessManagement.Serie.Create(serie);
                BusinessManagement.ActMedia.Create(new Dbo.ActMedia()
                {
                    IdMedia = serie.Id,
                    IdPerson = id,
                    Role = ""
                });
            }

            foreach (Dbo.Film film in list["Producer"].Item1)
            {
                BusinessManagement.Film.Create(film);
                BusinessManagement.ProduceMedia.Create(new Dbo.ProduceMedia()
                {
                    IdMedia = film.Id,
                    IdPerson = id,
                });
            }
            foreach (Dbo.Serie serie in list["Producer"].Item2)
            {
                BusinessManagement.Serie.Create(serie);
                BusinessManagement.ProduceMedia.Create(new Dbo.ProduceMedia()
                {
                    IdMedia = serie.Id,
                    IdPerson = id,
                });
            }

            foreach (Dbo.Film film in list["Director"].Item1)
            {
                BusinessManagement.Film.Create(film);
                BusinessManagement.RealiseMedia.Create(new Dbo.RealiseMedia()
                {
                    IdMedia = film.Id,
                    IdPerson = id,
                });
            }
            foreach (Dbo.Serie serie in list["Director"].Item2)
            {
                BusinessManagement.Serie.Create(serie);
                BusinessManagement.RealiseMedia.Create(new Dbo.RealiseMedia()
                {
                    IdMedia = serie.Id,
                    IdPerson = id,
                });
            }
        }

        // Ajoute tous les acteurs et directeurs d'un film dans la base
        static public void FilmFillActors(string id)
        {
            var list = DataAccess.MyAPIFilms.Film.GetDetailledInfo(id).Result;
            foreach (var person in list["Actor"])
            {
                BusinessManagement.Person.Create(person);
                BusinessManagement.ActMedia.Create(new Dbo.ActMedia()
                {
                    IdMedia = id,
                    IdPerson = person.Id,
                    Role = ""
                });
            }
            foreach (var person in list["Director"])
            {
                BusinessManagement.Person.Create(person);
                BusinessManagement.RealiseMedia.Create(new Dbo.RealiseMedia()
                {
                    IdMedia = id,
                    IdPerson = person.Id,
                });
            }
        }

        // Ajoute tous les acteurs et directeurs d'une série dans la base
        static public void SerieFillActors(string id)
        {
            var list = DataAccess.MyAPIFilms.Serie.GetDetailledInfo(id).Result;
            foreach (var person in list["Actor"])
            {
                // TODO AJOUT DANS ACT_IN
                BusinessManagement.Person.Create(person);
                BusinessManagement.ActMedia.Create(new Dbo.ActMedia()
                {
                    IdMedia = id,
                    IdPerson = person.Id,
                    Role = ""
                });
            }
            foreach (var person in list["Director"])
            {
                BusinessManagement.Person.Create(person);
                BusinessManagement.RealiseMedia.Create(new Dbo.RealiseMedia()
                {
                    IdMedia = id,
                    IdPerson = person.Id,
                });
            }
        }
    }
}