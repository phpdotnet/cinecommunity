﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.BusinessManagement
{
    public class Film
    {
        private static DataAccess.Film dataAccess = new DataAccess.Film();

        public static List<Dbo.Film> GetList()
        {
            return dataAccess.GetList();
        }

        public static List<Dbo.Film> GetListByType(short typeid)
        {
            return dataAccess.GetListByType(typeid);
        }

        public static List<Dbo.Film> Search(string name)
        {
            return dataAccess.Search(name);
        }

        public static Dbo.Film GetId(string id)
        {
            return dataAccess.GetId(id);
        }

        public static bool Create(Dbo.Film film)
        {
            return dataAccess.Create(film);
        }

        public static bool Update(Dbo.Film film)
        {
            return dataAccess.Update(film);
        }

        public static bool Delete(string id)
        {
            return dataAccess.Delete(id);
        }
    }
}