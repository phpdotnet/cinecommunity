﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.BusinessManagement
{
    public class User
    {
        private static DataAccess.User dataAccess = new DataAccess.User();

        public static List<Dbo.User> GetList()
        {
            return dataAccess.GetList();
        }

        public static Dbo.User GetId(long id)
        {
            return dataAccess.GetId(id);
        }

        public static Dbo.User GetByUsername(string username)
        {
            return dataAccess.GetByUsername(username);
        }

        public static bool Create(Dbo.User user)
        {
            return dataAccess.Create(user);
        }

        public static bool Update(Dbo.User user)
        {
            return dataAccess.Update(user);
        }

        public static bool Delete(long id)
        {
            return dataAccess.Delete(id);
        }
    }
}