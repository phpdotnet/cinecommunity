﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.BusinessManagement
{
    public class Type
    {
        private static DataAccess.Type dataAccess = new DataAccess.Type();

        public static List<Dbo.Type> GetList()
        {
            return dataAccess.GetList();
        }

        public static Dbo.Type GetId(short id)
        {
            return dataAccess.GetId(id);
        }

        public static bool Craete(Dbo.Type type)
        {
            return dataAccess.Create(type);
        }

        public static bool Update(Dbo.Type type)
        {
            return dataAccess.Update(type);
        }

        public static bool Delete(short id)
        {
            return dataAccess.Delete(id);
        }
    }
}