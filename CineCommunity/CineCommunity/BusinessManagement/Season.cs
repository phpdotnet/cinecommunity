﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.BusinessManagement
{
    public class Season
    {
        private static DataAccess.Season dataAccess = new DataAccess.Season();

        public static List<Dbo.Season> GetList(string idSerie)
        {
            return dataAccess.GetList(idSerie);
        }

        public static Dbo.Season GetId(string id)
        {
            return dataAccess.GetId(id);
        }

        public static bool Create(Dbo.Season season)
        {
            return dataAccess.Create(season);
        }

        public static bool Update(Dbo.Season season)
        {
            return dataAccess.Update(season);
        }

        public static bool Delete(string id)
        {
            return dataAccess.Delete(id);
        }
    }
}