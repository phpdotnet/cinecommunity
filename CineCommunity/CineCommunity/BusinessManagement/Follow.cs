﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.BusinessManagement
{
    public class Follow
    {
        private static DataAccess.Follow dataAccess = new DataAccess.Follow();

        public static bool Create(Dbo.Follow follow)
        {
            return dataAccess.Create(follow);
        }

        public static bool Update(Dbo.Follow follow)
        {
            return dataAccess.Update(follow);
        }

        public static bool Delete(long id)
        {
            return dataAccess.Delete(id);
        }

        public static Dbo.Follow GetId(long id)
        {
            return dataAccess.GetId(id);
        }

        public static Dbo.Follow GetFollow(Dbo.Follow follow)
        {
            return dataAccess.GetFollow(follow);
        }

        public static int GetFollowersCount(string id_following, bool is_person)
        {
            return dataAccess.GetFollowersCount(id_following, is_person);
        }
    }
}