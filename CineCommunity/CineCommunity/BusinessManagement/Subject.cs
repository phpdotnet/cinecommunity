﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.BusinessManagement
{
    public class Subject
    {
        private static DataAccess.Subject dataAccess = new DataAccess.Subject();

        public static List<Dbo.Subject> GetList()
        {
            return dataAccess.GetList();
        }

        public static List<Dbo.Subject> GetList(int count)
        {
            return dataAccess.GetList(count);
        }

        public static Dbo.Subject GetId(long id)
        {
            return dataAccess.GetId(id);
        }

        public static Dbo.Subject GetByTitle(string title)
        {
            return dataAccess.GetByTitle(title);
        }

        public static bool Create(Dbo.Subject subject)
        {
            return dataAccess.Create(subject);
        }

        public static bool Update(Dbo.Subject subject)
        {
            return dataAccess.Update(subject);
        }

        public static bool Delete(long id)
        {
            return dataAccess.Delete(id);
        }
    }
}