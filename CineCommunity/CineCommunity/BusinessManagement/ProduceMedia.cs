﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.BusinessManagement
{
    public class ProduceMedia
    {
        private static DataAccess.ProduceMedia dataAccess = new DataAccess.ProduceMedia();

        public static bool Create(Dbo.ProduceMedia produce_media)
        {
            return dataAccess.Create(produce_media);
        }

        public static bool Update(Dbo.ProduceMedia produce_media)
        {
            return dataAccess.Update(produce_media);
        }
    }
}