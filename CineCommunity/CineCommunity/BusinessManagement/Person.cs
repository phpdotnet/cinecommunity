﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.BusinessManagement
{
    public class Person
    {
        private static DataAccess.Person dataAccess = new DataAccess.Person();

        public static List<Dbo.Person> GetList()
        {
            return dataAccess.GetList();
        }

        public static Dbo.Person GetId(string id)
        {
            return dataAccess.GetId(id);
        }

        public static int GetRolesCount(string id)
        {
            return dataAccess.GetFilmsAct(id).Count + dataAccess.GetSeriesAct(id).Count;
        }

        public static bool Create(Dbo.Person person)
        {
            return dataAccess.Create(person);
        }

        public static bool Update(Dbo.Person person)
        {
            return dataAccess.Update(person);
        }

        public static bool Delete(string id)
        {
            return dataAccess.Delete(id);
        }
    }
}