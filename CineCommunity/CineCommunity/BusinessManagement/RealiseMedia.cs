﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.BusinessManagement
{
    public class RealiseMedia
    {
        private static DataAccess.RealiseMedia dataAccess = new DataAccess.RealiseMedia();

        public static bool Create(Dbo.RealiseMedia realise_media)
        {
            return dataAccess.Create(realise_media);
        }

        public static bool Update(Dbo.RealiseMedia realise_media)
        {
            return dataAccess.Update(realise_media);
        }
    }
}