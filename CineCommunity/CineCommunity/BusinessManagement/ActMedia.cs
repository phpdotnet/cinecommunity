﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.BusinessManagement
{
    public class ActMedia
    {
        private static DataAccess.ActMedia dataAccess = new DataAccess.ActMedia();

        public static bool Create(Dbo.ActMedia act_media)
        {
            return dataAccess.Create(act_media);
        }

        public static bool Update(Dbo.ActMedia act_media)
        {
            return dataAccess.Update(act_media);
        }
    }
}