﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.BusinessManagement
{
    public class Episode
    {
        private static DataAccess.Episode dataAccess = new DataAccess.Episode();

        public static List<Dbo.Episode> GetList(string idSeason)
        {
            return dataAccess.GetList(idSeason);
        }

        public static Dbo.Episode GetId(string id)
        {
            return dataAccess.GetId(id);
        }

        public static bool Create(Dbo.Episode episode)
        {
            return dataAccess.Create(episode);
        }

        public static bool Update(Dbo.Episode episode)
        {
            return dataAccess.Update(episode);
        }

        public static bool Delete(string id)
        {
            return dataAccess.Delete(id);
        }
    }
}