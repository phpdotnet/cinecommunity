﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.BusinessManagement
{
    public class Serie
    {
        private static DataAccess.Serie dataAccess = new DataAccess.Serie();

        public static List<Dbo.Serie> GetList()
        {
            return dataAccess.GetList();
        }

        public static List<Dbo.Serie> GetListByType(short typeid)
        {
            return dataAccess.GetListByType(typeid);
        }

        public static List<Dbo.Serie> Search(string name)
        {
            return dataAccess.Search(name);
        }

        public static Dbo.Serie GetId(string id)
        {
            return dataAccess.GetId(id);
        }

        public static bool Create(Dbo.Serie serie)
        {
            return dataAccess.Create(serie);
        }

        public static bool Update(Dbo.Serie serie)
        {
            return dataAccess.Update(serie);
        }

        public static bool Delete(string id)
        {
            return dataAccess.Delete(id);
        }
    }
}