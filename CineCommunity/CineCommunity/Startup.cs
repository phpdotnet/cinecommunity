﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CineCommunity.Startup))]
namespace CineCommunity
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
            ConfigureAuth(app);
        }
    }
}
