﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace CineCommunity
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ServiceClientCineCommunity" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ServiceClientCineCommunity.svc or ServiceClientCineCommunity.svc.cs at the Solution Explorer and start debugging.
    public class ServiceClientCineCommunity : IServiceClientCineCommunity
    {
        public KeyValuePair<string, int>[] ComputeMostLikedGenreByAge(string genre)
        {
            KeyValuePair<string, int>[] kvp = null;

            switch (genre)
            {
                case "Fantasy":
                    kvp = new KeyValuePair<string, int>[] {
                                    new KeyValuePair<string, int>("< 15 years old", 10),
                                    new KeyValuePair<string, int>("15-25 years old", 60),
                                    new KeyValuePair<string, int>("25-40 years old", 30) };
                    break;
                case "Horror":
                    kvp = new KeyValuePair<string, int>[] {
                                    new KeyValuePair<string, int>("< 15 years old", 60),
                                    new KeyValuePair<string, int>("15-25 years old", 30),
                                    new KeyValuePair<string, int>("25-40 years old", 10) };
                    break;
                default:
                    break;
            }

            return kvp;
        }
    }
}
