﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CineCommunity.Controllers
{
    public class CommunityController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.PopularCommunity = BusinessManagement.Subject.GetList(6);
            ViewBag.MostReviewsCommunity = BusinessManagement.Subject.GetList(6);
            return View();
        }

        public ActionResult Details(long id)
        {
            ViewBag.Community = BusinessManagement.Subject.GetId(id);

            return View();
        }
    }
}