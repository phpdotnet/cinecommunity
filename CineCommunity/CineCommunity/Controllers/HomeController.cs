﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CineCommunity.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<Dbo.Film> popularMovies = new List<Dbo.Film>();
            foreach (Dbo.Film movie in BusinessManagement.Film.GetList())
            {
                popularMovies.Add(movie);
            }
            popularMovies.Sort(compMovies);
            int range = Math.Min(3, popularMovies.Count);
            popularMovies.RemoveRange(range, popularMovies.Count - range);
            ViewBag.PopularMovies = popularMovies;

            List<Dbo.Serie> popularSeries = new List<Dbo.Serie>();
            foreach (Dbo.Serie serie in BusinessManagement.Serie.GetList())
            {
                popularSeries.Add(serie);
            }
            popularSeries.Sort(compSeries);
            range = Math.Min(3, popularSeries.Count);
            popularSeries.RemoveRange(range, popularSeries.Count - range);
            ViewBag.PopularSeries = popularSeries;

            List<Dbo.Person> popularCelebrities = new List<Dbo.Person>();
            foreach (Dbo.Person person in BusinessManagement.Person.GetList())
            {
                popularCelebrities.Add(person);
            }
            popularCelebrities.Sort(compPersons);
            range = Math.Min(3, popularCelebrities.Count);
            popularCelebrities.RemoveRange(range, popularCelebrities.Count - range);
            ViewBag.PopularCelebrities = popularCelebrities;

            return View();
        }

        public int compMovies(Dbo.Film a, Dbo.Film b)
        {
            return BusinessManagement.Follow.GetFollowersCount(b.Id, false) - BusinessManagement.Follow.GetFollowersCount(a.Id, false);
        }

        public int compSeries(Dbo.Serie a, Dbo.Serie b)
        {
            return BusinessManagement.Follow.GetFollowersCount(b.Id, false) - BusinessManagement.Follow.GetFollowersCount(a.Id, false);
        }

        public int compPersons(Dbo.Person a, Dbo.Person b)
        {
            return BusinessManagement.Follow.GetFollowersCount(b.Id, true) - BusinessManagement.Follow.GetFollowersCount(a.Id, true);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}