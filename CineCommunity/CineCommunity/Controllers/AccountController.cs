﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using CineCommunity.Models;
using System.Web.Security;
using System.Security.Policy;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Drawing;

namespace CineCommunity.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        public AccountController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

        public AccountController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }

        //
        // GET: /Account/Login
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(string returnUrl = "")
        {
            if (User.Identity.IsAuthenticated)
                return LogOut();

            ViewBag.ReturnUrl = returnUrl;
            LoginModel model = new LoginModel() { LoginViewModel = new LoginViewModel(), RegisterViewModel = new RegisterViewModel() };
            return View(model);
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.Username, model.Password))
                    FormsAuthentication.RedirectFromLoginPage(model.Username, model.RememberMe);

                ModelState.AddModelError("Password", "Incorrect username and/or password");
            }

            // If we got this far, something failed, redisplay form
            return View(new LoginModel() { LoginViewModel = model, RegisterViewModel = new RegisterViewModel()});
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account", null);
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                if (model.Password != model.ConfirmPassword)
                {
                    ModelState.AddModelError("Password", "Confirm password is different from password");
                    return View("Login", new LoginModel() { LoginViewModel = new LoginViewModel(), RegisterViewModel = model });
                }

                SHA256Managed hashstring = new SHA256Managed();
                model.Password = Encoding.UTF8.GetString(hashstring.ComputeHash(Encoding.UTF8.GetBytes(model.Password)));
                Dbo.User user = new Dbo.User()
                {
                    lastname = model.Lastname,
                    firstname = model.Firstname,
                    gender = model.Gender == "Homme",
                    birth_date = model.Birthdate,
                    email = model.Email,
                    username = model.Username,
                    password = model.Password
                };
                bool success = BusinessManagement.User.Create(user);

                if (success)
                {
                    FormsAuthentication.RedirectFromLoginPage(model.Username, false);
                }
                else
                {
                    ModelState.AddModelError("Username", "username already existing.");
                }
                return View("Login", new LoginModel() { LoginViewModel = new LoginViewModel(), RegisterViewModel = model });
            }

            // If we got this far, something failed, redisplay form
            return View("Login", new LoginModel() { LoginViewModel = new LoginViewModel(), RegisterViewModel = model });
        }

        [HttpPost]
        public async Task<ActionResult> UploadImage(HttpPostedFileBase image)
        {
            if (image != null)
            {
                string pic = System.IO.Path.GetRandomFileName() + Path.GetExtension(image.FileName);
                string path = System.IO.Path.Combine(Server.MapPath("~/Download"), pic);

                image.SaveAs(path);

                Dbo.User user = BusinessManagement.User.GetByUsername(User.Identity.Name);
                if (user.photo != null && user.photo != "")
                    System.IO.File.Delete(user.photo);
                
                user.photo = path;
                BusinessManagement.User.Update(user);
            }
            return RedirectToAction("Manage", "Account");
        }

        [HttpPost]
        public async Task<ActionResult> ChangePassword(ManageUserViewModel model)
        {
            Dbo.User user = BusinessManagement.User.GetByUsername(User.Identity.Name);
            if (ModelState.IsValid)
            {
                SHA256Managed hashstring = new SHA256Managed();
                model.OldPassword = Encoding.UTF8.GetString(hashstring.ComputeHash(Encoding.UTF8.GetBytes(model.OldPassword)));
                if (model.OldPassword == user.password)
                {
                    if (model.ConfirmPassword == model.NewPassword)
                    {
                        string password = Encoding.UTF8.GetString(hashstring.ComputeHash(Encoding.UTF8.GetBytes(model.NewPassword)));
                        user.password = password;
                        BusinessManagement.User.Update(user);
                    }
                    else
                    {
                        ModelState.AddModelError("ConfirmPassword", "Confirm Password is different from New Password");
                    }
                }
                else
                {
                    ModelState.AddModelError("OldPassword", "Invalid password");
                }
            }

            string gender = (bool) user.gender ? "Homme" : "Femme";
            string photo = user.photo;
            if (photo == null || photo == "")
                photo = "~/Content/Image/Unknown.png";

            using (MemoryStream ms = new MemoryStream())
            {
                Image image = Image.FromFile(photo);
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                photo = Convert.ToBase64String(ms.ToArray());
            }

            UserViewModel userViewModel = new UserViewModel()
            {
                Birthdate = user.birth_date,
                Email = user.email,
                Firstname = user.firstname,
                Lastname = user.lastname,
                Gender = gender,
                Password = user.password,
                Photo = photo,
                Username = user.username,
            };
            ManageViewModel manageViewModel = new ManageViewModel()
            {
                UserViewModel = userViewModel,
                ManageUserViewModel = model
            };
            return View("Manage", manageViewModel);
        }

        //
        // POST: /Account/Disassociate
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Disassociate(string loginProvider, string providerKey)
        {
            ManageMessageId? message = null;
            IdentityResult result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage
        public ActionResult Manage(ManageMessageId? message)
        {
            Dbo.User user = BusinessManagement.User.GetByUsername(User.Identity.Name);
            string gender = (bool) user.gender ? "Homme" : "Femme";
            string photo = user.photo;
            if (photo == null || photo == "")
                photo = Server.MapPath("~") + "Content/Image/Unknown.png";
            
            using (MemoryStream ms = new MemoryStream())
            {
                Image image = Image.FromFile(photo);
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                photo = Convert.ToBase64String(ms.ToArray());
            }

            UserViewModel userViewModel = new UserViewModel()
            {
                Birthdate = user.birth_date,
                Email = user.email,
                Firstname = user.firstname,
                Lastname = user.lastname,
                Gender = gender,
                Password = user.password,
                Photo = photo,
                Username = user.username,
            };
            ManageViewModel model = new ManageViewModel()
            {
                UserViewModel = userViewModel,
                ManageUserViewModel = new ManageUserViewModel()
            };
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            ViewBag.HasLocalPassword = HasPassword();
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View(model);
        }

        //
        // POST: /Account/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Save(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                Dbo.User user = BusinessManagement.User.GetByUsername(User.Identity.Name);
                user.lastname = model.Lastname;
                user.firstname = model.Firstname;
                user.email = model.Email;
                user.birth_date = model.Birthdate;
                user.gender = model.Gender == "Homme";

                BusinessManagement.User.Update(user);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var user = await UserManager.FindAsync(loginInfo.Login);
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // If the user does not have an account, then prompt the user to create an account
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { UserName = loginInfo.DefaultUserName });
            }
        }

        //
        // POST: /Account/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new ChallengeResult(provider, Url.Action("LinkLoginCallback", "Account"), User.Identity.GetUserId());
        }

        //
        // GET: /Account/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            if (result.Succeeded)
            {
                return RedirectToAction("Manage");
            }
            return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser() { UserName = model.UserName };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInAsync(user, isPersistent: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult RemoveAccountList()
        {
            var linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId());
            ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
            return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri) : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}