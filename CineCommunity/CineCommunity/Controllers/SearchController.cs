﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Threading.Tasks;

namespace CineCommunity.Controllers
{
    public class SearchController : Controller
    {
        public ActionResult Index(string q)
        {
            ViewBag.Search = q;

            var list = SearchMedias(q).Result;
            foreach (Dbo.Film film in list.Item1)
            {
                BusinessManagement.DatabaseFiller.FilmFill(film);
            }
            foreach (Dbo.Serie serie in list.Item2)
            {
                BusinessManagement.DatabaseFiller.SerieFill(serie);
            }

            ViewBag.Result = new Tuple<List<Dbo.Film>, List<Dbo.Serie>>(BusinessManagement.Film.Search(q), BusinessManagement.Serie.Search(q));

            return View();
        }

        public async Task<Tuple<List<Dbo.Film>, List<Dbo.Serie>>> SearchMedias(string q)
        {
            Tuple<List<Dbo.Film>, List<Dbo.Serie>> list = await DataAccess.MyAPIFilms.Search.SearchMedia(q).ConfigureAwait(false);
            return list;
        }
    }
}