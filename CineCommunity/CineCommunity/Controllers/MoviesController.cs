﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CineCommunity.Controllers
{
    public class MoviesController : Controller
    {
        public ActionResult Index()
        {
            Dictionary<string, List<Dbo.Film>> movies = new Dictionary<string, List<Dbo.Film>>();
            List<string> already = new List<string>();
            foreach (var type in BusinessManagement.Type.GetList())
            {
                List<Dbo.Film> temp = BusinessManagement.Film.GetListByType(type.id);
                if (temp.Count > 0)
                {
                    movies.Add(type.name, temp);
                    foreach (Dbo.Film film in temp)
                    {
                        already.Add(film.Id);
                    }
                }
            }
            List<Dbo.Film> notype = new List<Dbo.Film>();
            foreach (var film in BusinessManagement.Film.GetList())
            {
                if (!already.Contains(film.Id))
                    notype.Add(film);
            }
            movies.Add("No Type", notype);
            ViewBag.Movies = movies;
            return View();
        }

        public ActionResult Details(string id)
        {
            long user_id = 0;
            ViewBag.movie = BusinessManagement.Film.GetId(id);

            bool follow = BusinessManagement.Follow.GetFollow(new Dbo.Follow()
                {
                    IdFollower = user_id,
                    IdFollowing = id,
                    IsFollowingPerson = false
                }) != null;

            ViewBag.follow = follow;

            return View();
        }

        public ActionResult Follow(string id)
        {
            long user_id = 0;
            Dbo.Follow new_follow = new Dbo.Follow()
            {
                IdFollower = user_id,
                IdFollowing = id,
                IsFollowingPerson = false
            };

            Dbo.Follow follow = BusinessManagement.Follow.GetFollow(new_follow);
            if (follow == null)
            {
                BusinessManagement.Follow.Create(new_follow);
            }

            Response.Redirect("/movies/details?id=" + id);
            return View();
        }

        public ActionResult Unfollow(string id)
        {
            long user_id = 0;
            Dbo.Follow current_follow = new Dbo.Follow()
            {
                IdFollower = user_id,
                IdFollowing = id,
                IsFollowingPerson = false
            };
            Dbo.Follow follow = BusinessManagement.Follow.GetFollow(current_follow);
            if (follow != null)
            {
                BusinessManagement.Follow.Delete(follow.Id);
            }

            Response.Redirect("/movies/details?id=" + id);
            return View();
        }
    }
}