﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CineCommunity.Controllers
{
    public class CelebritiesController : Controller
    {
        public ActionResult Index()
        {
            List<Tuple<int, Dbo.Person>> popular = new List<Tuple<int, Dbo.Person>>();
            foreach (Dbo.Person person in BusinessManagement.Person.GetList())
            {
                popular.Add(new Tuple<int, Dbo.Person>(BusinessManagement.Follow.GetFollowersCount(person.Id, true), person));
            }
            popular.Sort(comp);

            List<Tuple<int, Dbo.Person>> mostroles = new List<Tuple<int, Dbo.Person>>();
            foreach (Dbo.Person person in BusinessManagement.Person.GetList())
            {
                mostroles.Add(new Tuple<int, Dbo.Person>(BusinessManagement.Person.GetRolesCount(person.Id), person));
            }
            mostroles.Sort(comp);

            ViewBag.PopularCelebrities = popular;
            ViewBag.MostRolesCelebrities = mostroles;
            return View();
        }

        public int comp(Tuple<int, Dbo.Person> a, Tuple<int, Dbo.Person> b)
        {
            return b.Item1 - a.Item1;
        }

        public ActionResult Details(string id)
        {
            long user_id = 0;
            ViewBag.person = BusinessManagement.Person.GetId(id);

            bool follow = BusinessManagement.Follow.GetFollow(new Dbo.Follow()
            {
                IdFollower = user_id,
                IdFollowing = id,
                IsFollowingPerson = true
            }) != null;

            ViewBag.follow = follow;

            return View();
        }

        public ActionResult Follow(string id)
        {
            long user_id = 0;
            Dbo.Follow new_follow = new Dbo.Follow()
            {
                IdFollower = user_id,
                IdFollowing = id,
                IsFollowingPerson = true
            };

            Dbo.Follow follow = BusinessManagement.Follow.GetFollow(new_follow);
            if (follow == null)
            {
                BusinessManagement.Follow.Create(new_follow);
            }

            Response.Redirect("/celebrities/details?id=" + id);
            return View();
        }

        public ActionResult Unfollow(string id)
        {
            long user_id = 0;
            Dbo.Follow current_follow = new Dbo.Follow()
            {
                IdFollower = user_id,
                IdFollowing = id,
                IsFollowingPerson = true
            };
            Dbo.Follow follow = BusinessManagement.Follow.GetFollow(current_follow);
            if (follow != null)
            {
                BusinessManagement.Follow.Delete(follow.Id);
            }

            Response.Redirect("/celebrities/details?id=" + id);
            return View();
        }
    }
}