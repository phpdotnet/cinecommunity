﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CineCommunity.Controllers
{
    public class SeriesController : Controller
    {
        public ActionResult Index()
        {
            Dictionary<string, List<Dbo.Serie>> series = new Dictionary<string, List<Dbo.Serie>>();
            List<string> already = new List<string>();
            foreach (var type in BusinessManagement.Type.GetList())
            {
                List<Dbo.Serie> temp = BusinessManagement.Serie.GetListByType(type.id);
                if (temp.Count > 0)
                {
                    series.Add(type.name, temp);
                    foreach (Dbo.Serie serie in temp)
                    {
                        already.Add(serie.Id);
                    }
                }
            }
            List<Dbo.Serie> notype = new List<Dbo.Serie>();
            foreach (var film in BusinessManagement.Serie.GetList())
            {
                if (!already.Contains(film.Id))
                    notype.Add(film);
            }
            series.Add("No Type", notype);
            ViewBag.Series = series;
            return View();
        }

        public ActionResult Details(string id)
        {
            long user_id = 0;
            ViewBag.serie = BusinessManagement.Serie.GetId(id);

            bool follow = BusinessManagement.Follow.GetFollow(new Dbo.Follow()
            {
                IdFollower = user_id,
                IdFollowing = id,
                IsFollowingPerson = false
            }) != null;

            ViewBag.follow = follow;

            return View();
        }

        public ActionResult Follow(string id)
        {
            long user_id = 0;
            Dbo.Follow new_follow = new Dbo.Follow()
            {
                IdFollower = user_id,
                IdFollowing = id,
                IsFollowingPerson = false
            };

            Dbo.Follow follow = BusinessManagement.Follow.GetFollow(new_follow);
            if (follow == null)
            {
                BusinessManagement.Follow.Create(new_follow);
            }

            Response.Redirect("/series/details?id=" + id);
            return View();
        }

        public ActionResult Unfollow(string id)
        {
            long user_id = 0;
            Dbo.Follow current_follow = new Dbo.Follow()
            {
                IdFollower = user_id,
                IdFollowing = id,
                IsFollowingPerson = false
            };
            Dbo.Follow follow = BusinessManagement.Follow.GetFollow(current_follow);
            if (follow != null)
            {
                BusinessManagement.Follow.Delete(follow.Id);
            }

            Response.Redirect("/series/details?id=" + id);
            return View();
        }
    }
}