﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace CineCommunity
{
    [ServiceContract]
    public interface IServiceClientCineCommunity
    {
        [OperationContract]
        KeyValuePair<string, int>[] ComputeMostLikedGenreByAge(string genre);
    }
}
