﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.Dbo
{
    public class Type
    {
        public short id { get; set; }
        public string name { get; set; }

        public List<Film> films { get; set; }
        public List<Serie> series { get; set; }
    }
}