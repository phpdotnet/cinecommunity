﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.Dbo
{
    public class Subject
    {
        public long id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public System.DateTime create_date { get; set; }

        public List<Message> messages { get; set; }
    }
}