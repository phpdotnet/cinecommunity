﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.Dbo
{
    public class Season
    {
        public string Id { get; set; }
        public short Number { get; set; }

        public string IdSerie { get; set; }

        public List<Episode> Episodes { get; set; }
    }
}