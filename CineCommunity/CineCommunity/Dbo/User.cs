﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.Dbo
{
    public class User
    {
        public long id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public Nullable<bool> gender { get; set; }
        public Nullable<System.DateTime> birth_date { get; set; }
        public string photo { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
    }
}