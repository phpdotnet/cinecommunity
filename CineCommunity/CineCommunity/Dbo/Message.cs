﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.Dbo
{
    public class Message
    {
        public long id { get; set; }
        public string text { get; set; }
        public string username { get; set; }
        public long id_subject { get; set; }
        public long id_user { get; set; }
    }
}