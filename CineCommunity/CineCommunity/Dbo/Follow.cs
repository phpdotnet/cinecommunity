﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.Dbo
{
    public class Follow
    {
        public long Id { get; set; }
        public string IdFollowing { get; set; }
        public long IdFollower { get; set; }
        public bool IsFollowingPerson { get; set; }       
    }
}