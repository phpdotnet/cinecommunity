﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.Dbo
{
    public class Serie
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Synopsis { get; set; }
        public Nullable<System.DateTime> ReleaseDate { get; set; }
        public string UrlPhoto { get; set; }
        public double? Rating { get; set; }

        public Nullable<System.DateTime> EndDate { get; set; }
        public List<String> Types { get; set; }

        public List<Season> Seasons { get; set; }
    }
}