﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.Dbo
{
    public class ActMedia
    {
        public long Id { get; set; }
        public string IdPerson { get; set; }
        public string IdMedia { get; set; }
        public string Role { get; set; }       
    }
}