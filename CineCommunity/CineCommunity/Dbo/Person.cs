﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.Dbo
{
    public class Person
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Biography { get; set; }
        public string Height { get; set; }
        public DateTime? BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public string UrlPhoto { get; set; }

        public string RoleName { get; set; }

    }
}