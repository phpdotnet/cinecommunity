﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CineCommunity.Hubs
{
    public class ChatHub : Hub
    {
        public void Send(int subjectId, string username, string message)
        {
            Dbo.User user = BusinessManagement.User.GetByUsername(username);
            Clients.All.addNewMessageToChat(subjectId, user.username, message);
            Dbo.Message messageDbo = new Dbo.Message()
            {
                id_subject = subjectId,
                id_user = user.id,
                text = message
            };
            BusinessManagement.Message.Create(messageDbo);
        }

        public void CreateSubject(string title, string description, string username)
        {
            Dbo.User user = BusinessManagement.User.GetByUsername(username);
            Dbo.Subject subject = new Dbo.Subject()
            {
                title = title,
                description = description,
                create_date = DateTime.Now
            };
            BusinessManagement.Subject.Create(subject);
            subject = BusinessManagement.Subject.GetByTitle(title);
            Clients.All.addNewSubject(subject.id, subject.title, subject.description);
            Clients.Caller.openNewChat(subject.id, subject.title, subject.description);
        }

        public void GetSubjects()
        {
            List<Dbo.Subject> subjects = BusinessManagement.Subject.GetList();
            Clients.Caller.loadSubjects(subjects);
        }

        public void GetMessages(int idSubject)
        {
            List<Dbo.Message> messages = BusinessManagement.Message.GetList(idSubject);
            Clients.Caller.loadMessages(messages);
        }
    }
}